#include <stdio.h>

#define maxn 110

int main() {
	int rmb[maxn],rp[maxn],t[maxn];
	int f[maxn][maxn],g[maxn][maxn];
	int n,i,j,k;
	scanf("%d",&n);
	for (i=1; i<=n; i++) scanf("%d%d%d",&(rmb[i]),&(rp[i]),&(t[i]));
	int m,r; //m->rmb, r->rp
	scanf("%d%d",&m,&r);
	for (i=1; i<=n; i++) 
		for (j=m; j>=rmb[i]; j--)
			for (k=r; k>=rp[i]; k--)
				if (f[j-rmb[i]][k-rp[i]]+1>f[j][k]) {
					f[j][k]=f[j-rmb[i]][k-rp[i]]+1;
					g[j][k]=g[j-rmb[i]][k-rp[i]]+t[i];
				}
				else if (f[j-rmb[i]][k-rp[i]]+1==f[j][k] && g[j-rmb[i]][k-rp[i]]+t[i]<g[j][k]) 
					g[j][k]=g[j-rmb[i]][k-rp[i]]+t[i];
	int ans=-1,anstime;
	for (i=1; i<=m; i++)
		for (j=1; j<=r; j++)
			if ((f[i][j]>ans) || (f[i][j]==ans && g[i][j]<anstime)) {
				ans=f[i][j];
				anstime=g[i][j];
			}
	printf("%d\n",anstime);
}