#include <stdio.h>

#define maxn 11
#define max(p,q) ((p)>(q)?(p):(q))

int a[maxn][maxn],f[maxn][maxn][maxn][maxn]={0};

int main()
{
	int n,i,j;
	scanf("%d",&n);
	int x,y,value;
	scanf("%d%d%d",&x,&y,&value);
	while ((x!=0) && (y!=0) && (value!=0))
	{
		a[x][y]=value;
		scanf("%d%d%d",&x,&y,&value);
	}
	int p,q;
	f[1][1][1][1]=a[1][1];
	for (i=1; i<=n; i++)
		for (j=1; j<=n; j++)
			for (p=1; p<=n; p++)
				for (q=1; q<=n; q++)
					if (!(i==p && j==q) || (i==n && j==n)) 
					{
						f[i][j][p][q]=max(f[i-1][j][p][q-1],f[i][j][p][q]);
    					f[i][j][p][q]=max(f[i-1][j][p-1][q],f[i][j][p][q]);
					    f[i][j][p][q]=max(f[i][j-1][p-1][q],f[i][j][p][q]);
    					f[i][j][p][q]=max(f[i][j-1][p][q-1],f[i][j][p][q]);
					    f[i][j][p][q]+=a[i][j]+a[p][q];
					}
	printf("%d\n",f[n][n][n][n]-a[n][n]);
	return 0;
}