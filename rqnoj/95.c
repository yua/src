#include <stdio.h>

#define maxn 110
#define maxl 1010

int max(int p, int q) {
	return p>q?p:q;
}

int main() {
	int n,m,l;
	scanf("%d%d%d",&n,&m,&l);
	int time[maxn],rate[maxn];
	int i,j,k;
	for (i=1; i<=n; i++) scanf("%d%d",&(time[i]),&(rate[i]));
	int f[maxl][maxn];
	for (i=1; i<=n; i++) 
		for (j=m; j>0; j--)
			for (k=l; k>=time[i]; k--) 
				if (f[j-1][k-time[i]] || (j-1==0 && k-time[i]==0))
				f[j][k]=max(f[j][k],f[j-1][k-time[i]]+rate[i]);
	int ans=0;
	for (j=0; j<=l; j++) ans=max(ans,f[m][j]);
	printf("%d\n",ans);
}