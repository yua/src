{
/**************************************************************
    Problem: 2049
    User: dennisyang
    Language: Pascal
    Result: Accepted
    Time:2544 ms
    Memory:424 kb
****************************************************************/
}
{$inline on}
{$DEFINE ONLINE_JUDGE}
const
  maxn=10010;
  trans:array [false..true] of string=('No','Yes');

var
  tree:array [0..maxn] of record
                            child:array [0..1] of longint;
                            isroot,rev:boolean;
                            parent,d:longint;
                          end;
  order:string;
  n,m,x,y:longint;

procedure swap(var x,y:longint); inline;
var
  t:longint;

begin
  t:=x;
  x:=y;
  y:=t;
end;

procedure update(root:longint); inline;
begin
  if tree[root].rev then
    begin
      tree[root].rev:=false;
      tree[tree[root].child[0]].rev:=not tree[tree[root].child[0]].rev;
      tree[tree[root].child[0]].d:=1-tree[tree[root].child[0]].d;
      tree[tree[root].child[1]].rev:=not tree[tree[root].child[1]].rev;
      tree[tree[root].child[1]].d:=1-tree[tree[root].child[1]].d;
      swap(tree[root].child[0],tree[root].child[1]);
    end;
end;

procedure set_child(p,c,d:longint); inline;
begin
  tree[p].child[d]:=c;
  tree[c].parent:=p;
  tree[c].d:=d;
end;

procedure rotate(root:longint);
var
  p,d:longint;

begin
  p:=tree[root].parent;
  update(p);
  update(root);
  d:=tree[root].d;
  if tree[p].isroot then //no grand-parent
    begin
      tree[p].isroot:=false;
      tree[root].isroot:=true;
      tree[root].parent:=tree[p].parent;
    end
  else set_child(tree[p].parent,root,tree[p].d);
  set_child(p,tree[root].child[1-d],d);
  set_child(root,p,1-d);
end;

procedure splay(root:longint);
var
  k:longint;

begin
  while not tree[root].isroot do
    if tree[tree[root].parent].isroot then rotate(root) else
      begin
        update(tree[root].parent);
        if tree[tree[root].parent].d=tree[root].d then rotate(tree[root].parent) else
          rotate(root);
        rotate(root);
      end;
  update(root);
end;

procedure access(x:longint);
var
  tmp:longint;

begin
  tmp:=0;
  repeat
    splay(x);
    tree[tree[x].child[1]].isroot:=true;
    tree[tmp].isroot:=false;
    set_child(x,tmp,1);
    tmp:=x;
    x:=tree[x].parent;
  until x=0;
end;

procedure link(x,y:longint); inline;
begin
  access(x);
  splay(x);
  tree[x].rev:=not tree[x].rev;
  update(x);
  tree[x].parent:=y;
end;

procedure cut(x,y:longint); inline;
//There are only 2 situations: x is y's parent or y is x's parent
var
  c:longint;

begin
  access(x);
  splay(x);
  c:=y;
  while not tree[c].isroot do c:=tree[c].parent;
  //If x is y's parent
  if x<>c then
    begin
      tree[c].parent:=0;
      exit;
    end;
  //same same
  access(y);
  splay(y);
  c:=x;
  while not tree[c].isroot do c:=tree[c].parent;
  if y<>c then
    begin
      tree[c].parent:=0;
      exit;
    end;
end;

function findroot(x:longint):longint; inline;
begin
  access(x);
  splay(x);
  update(x);
  while tree[x].child[0]>0 do
    begin
      x:=tree[x].child[0];
      update(x);
    end;
  exit(x);
end;

procedure getstr(var order:string); inline;
var
  ch:char;

begin
  read(ch);
  order:='';
  while ch<>' ' do
    begin
      order:=order+ch;
      read(ch);
    end;
end;

begin
{$IFNDEF ONLINE_JUDGE}
  assign(input,'cave.in');
  assign(output,'cave.out');
  reset(input);
  rewrite(output);
{$ENDIF}
  readln(n,m);
  for x:=1 to n do tree[x].isroot:=true;
  repeat
    dec(m);
    getstr(order);
    readln(x,y);
    case order[1] of
      'Q':writeln(trans[findroot(x)=findroot(y)]);
      'C':link(x,y);
      'D':cut(x,y);
    end;
  until m=0;
{$IFNDEF ONLINE_JUDGE}
  close(input);
  close(output);
{$ENDIF}
end.
