#include <cstdio>

using namespace std;

const int maxm=1<<10;

bool two[maxm][maxm];
int state[maxm],sum[maxm],tot=0;
long long dp[10][100][maxm];

inline bool ok(int p, int q) { return (p&q) == 0; }
int bitcount(unsigned int x)
{
	x = (x & 0x55555555) + ((x>>1) & 0x55555555);
	x = (x & 0x33333333) + ((x>>2) & 0x33333333);
	x = (x & 0x0F0F0F0F) + ((x>>4) & 0x0F0F0F0F);
	x = (x & 0x00FF00FF) + ((x>>8) & 0x00FF00FF);
	x = (x & 0x0000FFFF) + ((x>>16) & 0x0000FFFF);
	return x;
}

int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for (int i=0; i<(1<<n); ++i)
		if (ok(i,i>>1)) 
		{
			state[tot]=i,sum[tot]=bitcount(i);
			if (sum[tot]<=m) ++tot;
		}
	for (int i=0; i<tot; ++i)
		for (int j=0; j<tot; ++j)
			if (ok(state[i],state[j]) && ok(state[i],state[j]>>1) && ok(state[i],state[j]<<1))
				two[i][j]=true;
	for (int i=0; i<tot; ++i)
		dp[1][sum[i]][i]=1;
	for (int i=2; i<=n; ++i)
		for (int j=0; j<tot; ++j)
			for (int k=0; k<tot; ++k)
				if (two[j][k])
				{
					for (int p=0; p<=m; ++p)
						if (i*n>=p && sum[j]+sum[k]<=p) 
							dp[i][p][j]+=dp[i-1][p-sum[j]][k];
				}
	long long ans=0;
	for (int i=0; i<tot; ++i) ans+=dp[n][m][i];
	printf("%lld\n",ans);
	return 0;
}
