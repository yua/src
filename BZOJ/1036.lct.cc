/**************************************************************
    Problem: 1036
    User: dennisyang
    Language: C++
    Result: Accepted
    Time:3736 ms
    Memory:7276 kb
****************************************************************/
 
#include <cstdio>
#include <queue>
#include <vector>
#include <algorithm>
 
using namespace std;
 
const int maxn = 3e5 + 10, inf = 1<<20;
 
vector<int> epool[maxn];
 
struct node {
    node *ch[2], *p;
    int val, max, sum;
    node() {}
    node(int v);
    inline bool dir() { return this == p->ch[1]; }
    inline bool isroot() { return !(this == p->ch[0] || this == p->ch[1]); }
    inline void update();
} *nil;
 
inline node::node(int v) {
    val = max = sum = v;
    ch[0] = ch[1] = p = nil;
}
 
inline void node::update() {
    max = std::max(val, std::max(ch[0]->max, ch[1]->max));
    sum = ch[0]->sum + ch[1]->sum + val;
}
 
void rotate(node *u) {
    node *p = u->p;
    bool d = u->dir();
    u->p = p->p;
    if (!p->isroot()) p->p->ch[p->dir()] = u;
    if (u->ch[d^1] != nil) u->ch[d^1]->p = p;
    p->p = u, p->ch[d] = u->ch[d^1], u->ch[d^1] = p;
    p->update();
}
 
void splay(node *u) {
    while (!u->isroot()) {
        if (u->p->isroot())
            rotate(u);
        else
            u->dir() == u->p->dir() ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
    }
    u->update();
}
 
node* expose(node *u) {
    node *v;
    for (v = nil; u != nil; u = u->p) {
        splay(u);
        u->ch[1] = v;
        (v = u)->update();
    }
    return v;
}
 
node *id[maxn];
 
void bfs(int s) {
    static queue<int> q;
    static bool vis[maxn];
    q.push(s);
    vis[s] = true;
    while (!q.empty()) {
        int a = q.front();
        q.pop();
        for (int i = 0; i < epool[a].size(); ++i) {
            int b = epool[a][i];
            if (!vis[b]) {
                q.push(b);
                vis[b] = true;
                id[b]->p = id[a];
            }
        }
    }
}
 
int main() {
    int n, m;
    scanf("%d", &n);
    nil = new node(-inf);
    nil->sum = 0;
    nil->ch[0] = nil->ch[1] = nil->p = nil;
    int a, b;
    for (int i = 1; i < n; ++i) {
        scanf("%d%d", &a, &b);
        epool[a].push_back(b);
        epool[b].push_back(a);
    }
    for (int i = 1; i <= n; ++i) {
        scanf("%d", &a);
        id[i] = new node(a);
    }
    bfs(1);
    scanf("%d", &m);
    char op[10];
    node *lca;
    while (m--) {
        scanf(" %s%d%d", op, &a, &b);
        switch (op[0]) {
            case 'C':
                splay(id[a]);
                id[a]->val = b;
                id[a]->update();
                break;
            case 'Q':
                expose(id[a]);
                lca = expose(id[b]);
                splay(id[a]);
                if (op[1] == 'M') printf("%d\n", max(lca->val, max(lca->ch[1]->max, id[a] == lca ? -inf : id[a]->max)));
                if (op[1] == 'S') printf("%d\n", lca->val + lca->ch[1]->sum + (id[a] == lca ? 0 : id[a]->sum));
                break;
        }
    }
    delete nil;
    for (int i = 1; i <= n; ++i) delete id[i];
    return 0;
}
