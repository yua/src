type
  treap=^node;
  node=record
         priority,sum,key:longint;
         lson,rson:treap;
       end;

var
  n,min,k,ans:longint;
  root,NULL:treap;
  order:char;

procedure init;
begin
  new(NULL);
  NULL^.priority:=maxlongint;
  NULL^.key:=0;
  NULL^.sum:=0;
  NULL^.lson:=NULL;
  NULL^.rson:=NULL;
  root:=NULL;
end;

procedure lr(var root:treap);
var
  tmp:treap;

begin
  tmp:=root;
  root:=root^.rson;
  tmp^.rson:=root^.lson;
  root^.lson:=tmp;
  root^.sum:=tmp^.sum;
  tmp^.sum:=tmp^.lson^.sum+tmp^.rson^.sum+1;
end;

procedure rr(var root:treap);
var
  tmp:treap;

begin
  tmp:=root;
  root:=root^.lson;
  tmp^.lson:=root^.rson;
  root^.rson:=tmp;
  root^.sum:=tmp^.sum;
  tmp^.sum:=tmp^.lson^.sum+tmp^.rson^.sum+1;
end;

procedure ins(k:longint; var root:treap);
begin
  if root=NULL then
    begin
      new(root);
      root^.lson:=NULL;
      root^.rson:=NULL;
      root^.sum:=1;
      root^.priority:=random(maxlongint);
      root^.key:=k;
      exit;
    end;
  inc(root^.sum);
  if k<root^.key then
    begin
      ins(k,root^.lson);
      if root^.lson^.priority<root^.priority then rr(root);
    end
  else
    begin
      ins(k,root^.rson);
      if root^.rson^.priority<root^.priority then lr(root);
    end;
end;

procedure add(k:longint; root:treap);
begin
  if root=NULL then exit;
  inc(root^.key,k);
  add(k,root^.lson);
  add(k,root^.rson);
end;

function del(var root:treap):longint;
var
  tmp:longint;

begin
  if root=NULL then exit(0);
  if root^.key<min then
    begin
      tmp:=root^.lson^.sum+1;
      root:=root^.rson;
      exit(tmp+del(root));
    end
  else
    begin
      tmp:=del(root^.lson);
      dec(root^.sum,tmp);
      exit(tmp);
    end;
end;

function find(k:longint; root:treap):longint;
begin
  if root=NULL then exit(0);
  if k=root^.lson^.sum+1 then exit(root^.key);
  if k<root^.lson^.sum+1 then exit(find(k,root^.lson));
  exit(find(k-root^.lson^.sum-1,root^.rson));
end;

begin
  randomize;
  readln(n,min);
  init;
  repeat
    dec(n);
    readln(order,k);
    case order of
      'I':if k>=min then ins(k,root);
      'A':add(k,root);
      'S':begin
            add(-k,root);
            inc(ans,del(root));
          end;
      'F':if k>root^.sum then writeln(-1) else writeln(find(root^.sum+1-k,root));
    end;
  until n=0;
  writeln(ans);
  dispose(NULL);
  dispose(root);
end.
