#include <cstdio>
#include <limits>
#include <algorithm>

using namespace std;

const int maxn=30000+10;

int n,size[maxn],son[maxn],w[maxn],father[maxn],dep[maxn],top[maxn];
int cnt,tot;

struct node
{
	int adj;
	node *next;
} edge[maxn<<1], *ehead[maxn];

void link(int p, int q)
{
	edge[cnt].adj=q;
	edge[cnt].next=ehead[p];
	ehead[p]=&edge[cnt++];
}

void dfs(int i)
{
	size[i]=1;
	son[i]=0;
	for (node *p=ehead[i]; p; p=p->next)
		if (p->adj != father[i])
		{
			father[p->adj]=i;
			dep[p->adj]=dep[i]+1;
			dfs(p->adj);
			size[i]+=size[p->adj];
			if (size[son[i]]<size[p->adj]) son[i]=p->adj;
		}
}

void split(int i, int tp)
{
	w[i]=++tot;
	top[i]=tp;
	if (son[i]) split(son[i],tp);
	for (node *p=ehead[i]; p; p=p->next)
		if (p->adj != father[i] && p->adj != son[i])
			split(p->adj,p->adj);
}

namespace SegmentTree
{
	struct node
	{
		int sum,max;
		node() { sum=max=0; }
	} tree[maxn<<2];
    
	inline int mid(int l, int r) { return (l+r)>>1; }
    
	inline void update(int idx)
	{
		tree[idx].max=max(tree[idx<<1].max,tree[idx<<1|1].max);
		tree[idx].sum=tree[idx<<1].sum+tree[idx<<1|1].sum;
	}
    
	void Insert(int idx, int l, int r, int pos, int val)
	{
		if (l==r)
		{
			tree[idx].sum=tree[idx].max=val;
			return;
		}
		if (pos<=mid(l,r)) Insert(idx<<1,l,mid(l,r),pos,val); else
			Insert(idx<<1|1,mid(l,r)+1,r,pos,val);
		update(idx);
	}
    
	int qsum(int idx, int l, int r, int p, int q)
	{
		if (p<=l && r<=q) return tree[idx].sum;
		int res=0;
		if (p<=mid(l,r)) res+=qsum(idx<<1,l,mid(l,r),p,q);
		if (q>mid(l,r)) res+=qsum(idx<<1|1,mid(l,r)+1,r,p,q);
		return res;
	}
    
	int qmax(int idx, int l, int r, int p, int q)
	{
		if (p<=l && r<=q) return tree[idx].max;
		int res=numeric_limits<int>::min();
		if (p<=mid(l,r)) res=max(res,qmax(idx<<1,l,mid(l,r),p,q));
		if (q>mid(l,r)) res=max(res,qmax(idx<<1|1,mid(l,r)+1,r,p,q));
		return res;
	}
    
	inline int Query(int p, int q, bool flag)
	{
		return flag ? qsum(1,1,tot,p,q) : qmax(1,1,tot,p,q);
	}
}

namespace lca
{
	inline void modify(int pos, int val)
	{
		SegmentTree::Insert(1,1,tot,w[pos],val);
	}
    
	int getsum(int x, int y)
	{
        int res=0;
        while (top[x]!=top[y])
        {
            if (dep[top[x]]<dep[top[y]]) swap(x,y);
            res+=SegmentTree::Query(w[top[x]],w[x],1);
            x=father[top[x]];
        }
        if (w[x]>w[y]) swap(x,y);
        return res+SegmentTree::Query(w[x],w[y],1);
	}
	
	int getmax(int x, int y)
	{
        int res=numeric_limits<int>::min();
        while (top[x]!=top[y])
        {
            if (dep[top[x]]<dep[top[y]]) swap(x,y);
            res=max(res,SegmentTree::Query(w[top[x]],w[x],0));
            x=father[top[x]];
        }
        if (w[x]>w[y]) swap(x,y);
        return max(res,SegmentTree::Query(w[x],w[y],0));
	}
}

int main()
{
	scanf("%d",&n);
	for (int i=1,p,q; i<n; ++i)
	{
		scanf("%d%d",&p,&q);
		link(p,q);
		link(q,p);
	}
    dep[1]=1;
	dfs(1);
	split(1,1);
	for (int i=1,tmp; i<=n; ++i)
	{
		scanf("%d",&tmp);
		lca::modify(i,tmp);
	}
	int m,p,q;
	char opt[10];
	scanf("%d",&m);
	while (m--)
	{
		scanf(" %s %d %d",opt,&p,&q);
		switch (opt[1])
		{
			case 'M':
				printf("%d\n",lca::getmax(p,q));
				break;
			case 'S':
				printf("%d\n",lca::getsum(p,q));
				break;
			case 'H':
				lca::modify(p,q);
				break;
		}
	}
	return 0;
}
