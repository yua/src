{$M 10000000}
{$inline on}

const
  maxn=100010;
var
  lc,rc,sum,mark:array [0..maxn shl 2] of longint;
  dep,fa,son,top,size,data,w:array [0..maxn] of longint;
  edge:array [0..maxn shl 1] of record
                                  adj,next:longint;
                                end;
  ehead:array [0..maxn shl 1] of longint;
  n,m,i,lt,rt,x,y,c,tot,edge_tot:longint;
  order:char;

procedure insedge(x,y:longint); inline;
begin
  inc(edge_tot);
  edge[edge_tot].adj:=y;
  edge[edge_tot].next:=ehead[x];
  ehead[x]:=edge_tot;
end;

procedure swap(var x,y:longint); inline;
var
  t:longint;

begin
  t:=x;
  x:=y;
  y:=t;
end;

procedure pushup(root:longint); inline;
begin
  lc[root]:=lc[root shl 1];
  rc[root]:=rc[root shl 1+1];
  sum[root]:=sum[root shl 1]+sum[root shl 1+1]-ord(rc[root shl 1]=lc[root shl 1+1]);
end;

procedure pushdown(root:longint); inline;
var
  lson,rson:longint;

begin
  if mark[root]>=0 then
    begin
      lson:=root shl 1;
      rson:=root shl 1+1;
      lc[lson]:=mark[root];
      rc[lson]:=mark[root];
      lc[rson]:=mark[root];
      rc[rson]:=mark[root];
      mark[lson]:=mark[root];
      mark[rson]:=mark[root];
      mark[root]:=-1;
      sum[lson]:=1;
      sum[rson]:=1;
    end;
end;

procedure ins(root,l,r,p,q,c:longint);
var
  m:longint;

begin
  if (l<=p) and (q<=r) then
    begin
      mark[root]:=c;
      lc[root]:=c;
      rc[root]:=c;
      sum[root]:=1;
      exit;
    end;
  m:=(p+q) shr 1;
  pushdown(root);
  if l<=m then ins(root shl 1,l,r,p,m,c);
  if r>m then ins(root shl 1+1,l,r,m+1,q,c);
  pushup(root);
end;

function query(root,l,r,p,q,c:longint):longint;
var
  t1,t2,tmp,m:longint;

begin
  if l=p then lt:=lc[root];
  if r=q then rt:=rc[root];
  if (l<=p) and (q<=r) then exit(sum[root]);
  pushdown(root);
  m:=(p+q) shr 1;
  tmp:=0;
  t1:=-1;
  t2:=-1;
  if l<=m then
    begin
      inc(tmp,query(root shl 1,l,r,p,m,c));
      t1:=rc[root shl 1];
    end;
  if r>m then
    begin
      inc(tmp,query(root shl 1+1,l,r,m+1,q,c));
      t2:=lc[root shl 1+1];
    end;
  dec(tmp,ord((t1=t2) and (t1<>-1)));
  exit(tmp);
end;

procedure change(x,y,c:longint);
begin
  while top[x]<>top[y] do
    begin
      if dep[top[x]]<dep[top[y]] then swap(x,y);
      ins(1,w[top[x]],w[x],1,tot,c);
      x:=fa[top[x]];
    end;
  if dep[x]<dep[y] then swap(x,y);
  ins(1,w[y],w[x],1,tot,c);
end;

function ask(x,y:longint):longint;
var
  t1,t2,tmp:longint;

begin
  t1:=-1;
  t2:=-1;
  tmp:=0;
  while top[x]<>top[y] do
    begin
      if dep[top[x]]<dep[top[y]] then
        begin
          swap(x,y);
          swap(t1,t2);
        end;
      inc(tmp,query(1,w[top[x]],w[x],1,tot,c));
      dec(tmp,ord(t1=rt));
      t1:=lt;
      x:=fa[top[x]];
    end;
  if dep[x]<dep[y] then
    begin
      swap(x,y);
      swap(t1,t2);
    end;
  inc(tmp,query(1,w[y],w[x],1,tot,c));
  dec(tmp,ord((t1=rt) and (t1<>-1)));
  dec(tmp,ord((t2=lt) and (t2<>-1)));
  exit(tmp);
end;

procedure dfs(root:longint);
var
  now:longint;

begin
  now:=ehead[root];
  size[root]:=1;
  son[root]:=0;
  while now>0 do
    begin
      if edge[now].adj<>fa[root] then
        begin
          fa[edge[now].adj]:=root;
          dep[edge[now].adj]:=dep[root]+1;
          dfs(edge[now].adj);
          inc(size[root],size[edge[now].adj]);
          if size[edge[now].adj]>size[son[root]] then son[root]:=edge[now].adj;
        end;
      now:=edge[now].next;
    end;
end;

procedure split(root,tp:longint);
var
  now:longint;

begin
  inc(tot);
  w[root]:=tot;
  top[root]:=tp;
  if son[root]>0 then split(son[root],top[root]);
  now:=ehead[root];
  while now>0 do
    begin
      if (edge[now].adj<>fa[root]) and (edge[now].adj<>son[root]) then
        split(edge[now].adj,edge[now].adj);
      now:=edge[now].next;
    end;
end;

begin
  readln(n,m);
  for i:=1 to n do read(data[i]);
  for i:=1 to n-1 do
    begin
      readln(x,y);
      insedge(x,y);
      insedge(y,x);
    end;
  dfs(1);
  split(1,1);
  fillchar(mark,sizeof(mark),255);
  for i:=1 to n do ins(1,w[i],w[i],1,tot,data[i]);
  repeat
    dec(m);
    read(order,x,y);
    if order='Q' then
      begin
        readln;
        writeln(ask(x,y));
      end
    else
      begin
        readln(c);
        change(x,y,c);
      end;
  until m=0;
end.
