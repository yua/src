/**************************************************************
	Problem: 1010
	User: dennisyang
	Language: C
	Result: Accepted
	Time:176 ms
	Memory:1732 kb
****************************************************************/
 
/*
dp[i] = min{dp[j-1] + (i - j + sigma(c[k],j<=k<=i) - L)^2}
 
s[i] = sigma(c[k],1<=k<=i) + i
l = L + 1
 
dp[i] = min{dp[j-1] + (s[i]-s[j-1]-l)^2}
dp[k-1]+(s[i]-s[k-1]-l)^2 < dp[j-1] + (s[i]-s[j-1]-l)^2
dp[k-1] + s[k-1]^2 - 2*s[i]*s[k-1] + 2*s[k-1]*l < dp[j-1] + s[j-1]^2 - 2*s[i]*s[j-1] + 2*s[j-1]*l
dp[k-1] + s[k-1]^2 + 2*s[k-1]*l - (dp[j-1] + s[j-1]^2 + 2*s[j-1]*l) < 2*s[i]*(s[k-1]-s[j-1])
y(k) = dp[k-1] + s[k-1]^2 + 2*s[k-1]*l
x(k) = 2*s[k-1]
y(k) - y(j) < s[i] * (x(k) - x(j))
 
slope = (y(k) - y(j)) / (x(k) - x(j))
 
j<k (x(k) > x(j))
	slope < s[i]
j>k (x(k) < x(j))
	slope > s[i]
*/
#include <stdio.h>
 
#define sqr(x) ((x)*(x))
#define maxn 50001
 
typedef long long int64;
 
int n,queue[maxn];
int64 l,s[maxn],dp[maxn];
 
inline double y(int p) {
	return dp[p-1] + sqr(s[p-1]) + 2*l*s[p-1];
}
 
inline double x(int p) {
	return 2*s[p-1];
}
 
inline double slope(int p, int q) {
	return (y(p) - y(q)) / (x(p) - x(q));
}
 
int main() {
	scanf("%d%lld",&n,&l);
	++l;
	int i;
	for (i=1; i<=n; ++i) scanf("%lld",s+i);
	for (i=1; i<=n; ++i) s[i] += s[i-1];
	for (i=1; i<=n; ++i) s[i] += i;
	int *qf = &queue[1], *qr = &queue[0];
	for (i=1; i<=n; ++i) {
		while (qf < qr && slope(*(qr-1),*qr) > slope(*qr,i)) --qr;
		*++qr = i;
		while (qf < qr && slope(*qf,*(qf+1)) < s[i]) ++qf;
		dp[i] = dp[*qf - 1] + sqr(s[i] - s[*qf - 1] - l);
	}
	printf("%lld\n",dp[n]);
	return 0;
}