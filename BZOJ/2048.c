#include <stdio.h>
#include <math.h>

#define ln(x) log(x)

typedef long long int64;

const double euler = 0.5772156649015328, eps = 1e-8;

int main() {
	int64 n,m;
	scanf("%lld%lld",&n,&m);
	if (n > 1e8) {
		printf("%lld\n",(int64)((ln(n) + euler) * m / 2 - eps));
		return 0;
	}
	double e = 0;
	int i;
	for (i = 1; i <= n; ++i) e += 1.0/i;
	printf("%lld\n",(int64)(e * m / 2 - eps));
	return 0;
}
