#include <cstdio>
#include <ext/rope>

__gnu_cxx::crope rope;

int tcase,now,len,i;
char opt[10],buffer[1<<20],ch;

int main()
{
	scanf("%d",&tcase);
	while (tcase--)
	{
		scanf(" %s",opt);
		switch (opt[0])
		{
			case 'M': scanf("%d",&now); break;
			case 'I':
				scanf("%d",&len);
				i=0;
				while (len)
				{
					ch=getchar();
					if (32<=ch && ch<=126) buffer[i++]=ch,--len;
				}
				buffer[i]='\0';
				rope.insert(now,buffer);
				break;
			case 'D':
				scanf("%d",&len);
				rope.erase(now,len);
				break;
			case 'G':
				scanf("%d",&len);
				puts(rope.substr(now,len).c_str());
				break;
			case 'P': --now; break;
			case 'N': ++now; break;
		}
	}
	return 0;
}
