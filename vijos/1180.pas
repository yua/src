const
  maxn=400;

type
  node=record
         l,r,score:longint; //l: lson, r: rson, sroce:..
       end;  

var
  n,m,father,i:longint;
  opt:array [0..maxn,0..maxn] of longint;
  f:array [0..maxn] of longint;
  tree:array [0..maxn] of record
                            l,r,score:longint; //l: lson, r: rson, sroce:..
                          end; 

function max(p,q:longint):longint;
begin
  if p>q then exit(p) else exit(q);
end;

function mdfs(now,need:longint):longint;
var
  i:longint;

begin
  if opt[now][need]>=0 then exit(opt[now][need]);
  if (tree[now].l<0) and (tree[now].r<0) then
    begin
      if need=1 then opt[now][need]:=tree[now].score else opt[now][need]:=0;
    end
  else if (tree[now].l>=0) and (tree[now].r<0) then
    opt[now][need]:=max(opt[now][need],mdfs(tree[now].l,need-1)+tree[now].score)
  else if (tree[now].l<0) and (tree[now].r>=0) then
    begin
      opt[now][need]:=max(opt[now][need],mdfs(tree[now].r,need));
      opt[now][need]:=max(opt[now][need],mdfs(tree[now].r,need-1)+tree[now].score);
    end
  else
    begin
      opt[now][need]:=max(opt[now][need],mdfs(tree[now].r,need));
      for i:=1 to need-1 do 
        opt[now][need]:=max(opt[now][need],mdfs(tree[now].l,i-1)+mdfs(tree[now].r,need-i)+tree[now].score);
    end;
  exit(opt[now][need]);
end;

begin
  readln(n,m);
  fillchar(opt,sizeof(opt),200);
  fillchar(f,sizeof(f),0);
  for i:=0 to n do 
    begin
      tree[i].l:=-1;
      tree[i].r:=-1;
      opt[i,0]:=0;
    end;
  for i:=1 to n do
    begin
      readln(father,tree[i].score);
      if f[father]=0 then tree[father].l:=i else tree[f[father]].r:=i;
      f[father]:=i;
    end;
  writeln(mdfs(0,m+1));
end.