#include <stdio.h>

#define maxn 3010

int a[maxn],f[maxn][maxn]={0};

int min(int p, int q, int r) {
	p=(p<q?p:q);
	p=(p<r?p:r);
	return p;
}

int query(int l, int r) {
	if (l>=r) return 0;
	if (f[l][r]) return f[l][r];
	if (a[l]==a[r]) return f[l][r]=query(l+1,r-1); else
		return f[l][r]=min(query(l+1,r),query(l,r-1),query(l+1,r-1))+1;
}

int main() {
	freopen("queue.in","r",stdin);
	freopen("queue.out","w",stdout);
	int n,i;
	scanf("%d",&n);
	for (i=1; i<=n; i++) scanf("%d",&(a[i]));
	printf("%d\n",query(1,n));
	return 0;
}