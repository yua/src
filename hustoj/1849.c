#include <stdio.h>

#define maxv 100
#define maxn 10010

int main() {
	int v,n,a[maxv];
	long long f[maxn];
	scanf("%d%d",&v,&n);
	int i,j;
	for (i=1; i<=v; i++) scanf("%d",&(a[i]));
	f[0]=1;
	for (i=1; i<=v; i++) 
		for (j=a[i]; j<=n; j++) 
			f[j]+=f[j-a[i]];
	printf("%lld\n",f[n]);
	return 0;
}