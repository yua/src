const
  maxlen=1000010;

type
  node=record
         failure,id,count:longint;
         next:array ['a'..'z'] of longint;
       end;

var
  s:array [0..maxlen] of char;
  p:array [0..150] of string;
  trie:array [0..500010] of node;
  ans:array [0..150] of longint;
  n,len,cnt:longint;

procedure ins(s:string; k:longint);
var
  i,p,len:longint;

begin
  p:=0;
  len:=length(s);
  for i:=1 to len do
    begin
      if trie[p].next[s[i]]=0 then
        begin
          inc(cnt);
          trie[p].next[s[i]]:=cnt;
        end;
      p:=trie[p].next[s[i]];
    end;
  if trie[p].id=0 then
    begin
      trie[p].id:=k;
      trie[p].count:=1;
    end
  else inc(trie[p].count);
end;

procedure init;
var
  i:longint;
  ch:char;

begin
  cnt:=0;
  fillchar(p,sizeof(p),0);
  fillchar(trie,sizeof(trie),0);
  fillchar(ans,sizeof(ans),0);
  for i:=1 to n do
    begin
      readln(p[i]);
      ins(p[i],i);
    end;
  len:=0;
  while not eoln do
    begin
      read(ch);
      inc(len);
      s[len]:=ch;
    end;
end;

procedure build;
var
  queue:array [0..500000] of longint;
  head,tail,u,tmp,p:longint;
  i:char;

begin
  head:=0;
  tail:=1;
  queue[1]:=0;
  trie[0].failure:=-1;
  repeat
    inc(head);
    u:=queue[head];
    for i:='a' to 'z' do
      begin
        tmp:=trie[u].next[i];
        if tmp>0 then
          begin
            p:=trie[u].failure;
            while p<>-1 do
              begin
                if trie[p].next[i]>0 then
                  begin
                    trie[tmp].failure:=trie[p].next[i];
                    break;
                  end;
                p:=trie[p].failure;
              end;
            if p=-1 then trie[tmp].failure:=0;
            inc(tail);
            queue[tail]:=tmp;
          end;
      end;
  until head>=tail;
end;

procedure solve;
var
  i,p,tmp:longint;

begin
  p:=0;
  for i:=1 to len do
    begin
      while (p>0) and (trie[p].next[s[i]]=0) do p:=trie[p].failure;
      p:=trie[p].next[s[i]];
      tmp:=p;
      while (tmp>0) do
        begin
          inc(ans[trie[tmp].id],trie[tmp].count);
          tmp:=trie[tmp].failure;
        end;
    end;
end;

procedure print;
var
  i,k:longint;

begin
  k:=0;
  for i:=1 to n do if ans[i]>ans[k] then k:=i;
  writeln(ans[k]);
  for i:=1 to n do
    if ans[i]=ans[k] then writeln(p[i]);
end;

begin
  readln(n);
  while n>0 do
    begin
      init;
      build;
      solve;
      print;
      readln(n);
    end;
end.