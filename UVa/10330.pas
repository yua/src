{$inline on}

const
  maxn=100;
  maxpoint=202;
  //0..200: points
  //201: Barisal, 202:Dhaka
  src=201;
  dest=202;

var
  n,m,i,ans,min:longint;
  a:array [0..maxpoint,0..maxpoint] of longint;
  pre:array [0..maxpoint] of longint;

function bfs(src,dest:longint):boolean;
var
  queue:array [0..maxpoint] of longint;
  vis:array [0..maxpoint] of boolean;
  head,tail,i,j:longint;

begin
  fillchar(queue,sizeof(queue),0);
  fillchar(vis,sizeof(vis),false);
  fillchar(pre,sizeof(pre),0);
  head:=0;
  tail:=1;
  queue[1]:=src;
  repeat
    inc(head);
    i:=queue[head];
    for j:=1 to maxpoint do
      if (not vis[j]) and (a[i,j]>0) then
        begin
          inc(tail);
          queue[tail]:=j;
          pre[j]:=i;
          vis[j]:=true;
          if j=dest then exit(true);
        end;
  until head>=tail;
  exit(false);
end;

procedure init; inline;
var
  n,m,i,k,b,d,p,q,c:longint;

begin
  readln(n);
  for i:=1 to n do
    begin
      read(k);
      a[i,i+maxn]:=k;
    end;
  readln(m);
  for i:=1 to m do
    begin
      readln(p,q,c);
      a[p+maxn,q]:=c;
    end;
  readln(b,d);
  for i:=1 to b do
    begin
      read(k);
      a[src,k]:=maxlongint;
    end;
  for i:=1 to d do
    begin
      read(k);
      a[k+maxn,dest]:=maxlongint;
    end;
  readln;
end;

begin
  while not eof do
    begin
      fillchar(a,sizeof(a),0);
      init;
      ans:=0;
      repeat
        if not bfs(src,dest) then break;
        i:=dest;
        min:=maxlongint;
        while i<>src do
          begin
            if a[pre[i],i]<min then min:=a[pre[i],i];
            i:=pre[i];
          end;
        inc(ans,min);
        i:=dest;
        while i<>src do
          begin
            dec(a[pre[i],i],min);
            inc(a[i,pre[i]],min);
            i:=pre[i];
          end;
      until false;
      writeln(ans);
    end;
end.
