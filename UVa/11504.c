#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define maxn 100010
#define maxm 100010
#define min(p,q) ((p)<(q)?(p):(q))

typedef struct _edge {
	int adj;
	struct _edge *next;
} edge;

edge epool[maxm],*ehead[maxn],*cnt;

void link(int p, int q) {
	cnt->adj = q;
	cnt->next = ehead[p];
	ehead[p] = cnt++;
}

struct {
	int x,y;
} e[maxm];

bool instack[maxn];
int dis[maxn],low[maxn],stack[maxn],tot,*top;
int indeg[maxn],sccno[maxn],scc_cnt;

void tarjan(int i) {
	dis[i] = low[i] = ++tot;

	*top++ = i;
	instack[i] = true;

	edge *p;
	for (p = ehead[i]; p != NULL; p = p->next) {
		int j = p->adj;
		if (!dis[j]) tarjan(j);
		if (instack[j]) low[i] = min(low[i],low[j]);
	}

	if (dis[i] == low[i]) {
		int j;
		++scc_cnt;
		do {
			j = *--top;
			instack[j] = false;
			sccno[j] = scc_cnt;
		} while (j != i);
	}
}

int main() {
	int tcase,n,m,i;
	scanf("%d",&tcase);
	while (tcase--) {
		scanf("%d%d",&n,&m);
		cnt = &epool[0];
		scc_cnt = 0;
		memset(e,0,sizeof e);
		memset(ehead,NULL,sizeof ehead);
		memset(epool,0,sizeof epool);
		memset(stack,0,sizeof stack);
		memset(instack,false,sizeof instack);
		memset(sccno,0,sizeof sccno);
		memset(dis,0,sizeof dis);
		for (i = 1; i <= m; ++i) {
			scanf("%d%d",&e[i].x,&e[i].y);
			link(e[i].x,e[i].y);
		}
		tot = 0;
		top = &stack[0];
		for (i = 1; i <= n; ++i) if (!dis[i]) tarjan(i);
		memset(indeg,0,sizeof indeg);
		for (i = 1; i <= m; ++i)
			if (sccno[e[i].x] != sccno[e[i].y]) ++indeg[sccno[e[i].y]];
		int ans = 0;
		for (i = 1; i <= scc_cnt; ++i) ans += (indeg[i] == 0);
		printf("%d\n",ans);
	}
	return 0;
}
