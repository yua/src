const
  maxn=5100001;

var
  i,n,t,len,ans,count:longint;
  st:array [0..maxn] of char;
  s:ansistring;
  trie:array [0..maxn,'A'..'Z'] of longint;
  flag,failure:array [0..maxn] of longint;
  queue:array [0..maxn] of longint;

procedure ins;
var
  p:longint;
  i:longint;

begin
  p:=0;
  for i:=1 to len do
    begin
      if trie[p,s[i]]=0 then
        begin
          inc(count);
          trie[p,s[i]]:=count;
        end;
      p:=trie[p,s[i]];
    end;
  flag[p]:=1;
end;

procedure build;
var
  head,tail:longint;
  tmp,u,p:longint;
  i:char;

begin
  failure[0]:=-1;
  head:=0;
  tail:=1;
  queue[1]:=0;
  repeat
    inc(head);
    u:=queue[head];
    for i:='A' to 'Z' do
      begin
        p:=trie[u,i];
        if p>0 then
          begin
            tmp:=failure[u];
            while tmp<>-1 do
              begin
                if trie[tmp,i]>0 then
                  begin
                    failure[p]:=trie[tmp,i];
                    break;
                  end;
                tmp:=failure[tmp];
              end;
            if tmp=-1 then failure[p]:=0;
            inc(tail);
            queue[tail]:=p;
          end;
      end;
  until head>=tail;
end;

procedure solve;
var
  i,p,tmp:longint;

begin
  p:=0;
  for i:=1 to len do
    begin
      while (p>0) and (trie[p,st[i]]=0) do p:=failure[p];
      p:=trie[p,st[i]];
      tmp:=p;
      while (tmp>0) and (flag[tmp]<>-1) do
        begin
          inc(ans,flag[tmp]);
          flag[tmp]:=-1;
          tmp:=failure[tmp];
        end;
    end;
end;

procedure swap(var p,q:char);
var
  t:char;

begin
  t:=p;
  p:=q;
  q:=t;
end;

procedure gets;
var
  ch:char;
  f:longint;

begin
  s:='';
  read(ch);
  f:=0;
  while not eoln do
    begin
      if ch='[' then
        begin
          f:=0;
          read(ch);
          while not (ch in ['A'..'Z']) do
            begin
              f:=f*10+ord(ch)-48;
              read(ch);
            end;
          repeat
            dec(f);
            inc(len);
            st[len]:=ch;
          until f=0;
          read(ch);
        end
      else
        begin
          inc(len);
          st[len]:=ch;
        end;
      read(ch);
    end;
  inc(len);
  st[len]:=ch;
end;

begin
  readln(t);
  repeat
    dec(t);
    fillchar(trie,sizeof(trie),0);
    fillchar(flag,sizeof(flag),0);
    fillchar(failure,sizeof(failure),0);
    count:=0;
    readln(n);
    for i:=1 to n do
      begin
        readln(s);
        len:=length(s);
        ins;
      end;
    build;
    len:=0;
    gets;
    ans:=0;
    solve;
    for i:=1 to len shr 1 do swap(st[i],st[len-i+1]);
    solve;
    writeln(ans);
  until t=0;
end.
