#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

#define in(x) scanf("%d",&(x))
#define out(x) printf("%d\n",x)

using namespace std;

const int maxn = 21, cnt = 1<<maxn;

int n, data[maxn][maxn], dp[2][cnt];

int *last, *cur;

vector<int> state;

void dfs(int dep, int s) {
    if (dep == n) {
        state.push_back(s);
        return;
    }
    dfs(dep + 1, s);
    if (s & (1 << (dep-1))) return;
    dfs(dep + 1, s | (1 << dep));
}

inline bool fit(int p, int q) { return (p & q) == 0; }

int cal(int state, int data[]) {
    int res = 0;
    for (int i = 0; i < n; ++i, state >>= 1)
        if (state & 1) res += data[i];
    return res;
}

int main() {
    while (in(n) != EOF) {
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                in(data[i][j]);
        memset(dp, 0, sizeof dp);
        state.clear();
        dfs(0, 0);
        for (int i = 0; i < state.size(); ++i) dp[0][i] = cal(state[i], data[0]);
        last = dp[0], cur = dp[1];
        for (int i = 1; i <= n; ++i) {
            memset(cur, 0, sizeof cur);
            for (int p = 0; p < state.size(); ++p) { //cur
                int tmp = cal(state[p], data[i]);
                for (int q = 0; q < state.size(); ++q) //last
                    if (fit(state[p], state[q])) cur[p] = max(cur[p], last[q] + tmp);
            }
            swap(last, cur);
        }
        out(last[0]);
    }
    return 0;
}
