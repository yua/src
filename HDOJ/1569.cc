#include <cstdio>
#include <cstring>
#include <algorithm>

using std::scanf;
using std::printf;
using std::min;
using std::memset;

const int maxn = 2510, maxm = maxn * 100, inf = 1<<30;
const int way[4][2] = {0, 1, 0, -1, 1, 0, -1, 0};

int n, m;

int to[maxm], next[maxm], cap[maxm], etot;
int adj[maxn], v;

void link(int a, int b, int c) {
	to[etot] = b;
	next[etot] = adj[a];
	cap[etot] = c;
	adj[a] = etot++;
	to[etot] = a;
	next[etot] = adj[b]++;
	cap[etot] = 0;
	adj[b] = etot++;
}

int h[maxn], gap[maxn];

int dfs(int a, int df, const int s, const int t) {
	if (a == t) return df;
	for (int i = adj[a]; i != -1; i = next[i]) {
		int b = to[i];
		if (cap[i] && h[a] == h[b] + 1) {
			int f = dfs(b, min(df, cap[i]), s, t);
			if (f) {
				cap[i] -= f;
				cap[i^1] += f;
				return f;
			}
		}
	}
	if (--gap[h[a]] == 0) h[s] = n * m + 2;
	++gap[++h[a]];
	return 0;
}

int isap(const int s, const int t) {
	memset(h, 0, sizeof h);
	memset(gap, 0, sizeof gap);
	int res = 0;
	while (h[s] < n * m + 2) res += dfs(s, inf, s, t);
	return res;
}

inline bool even(int x) { return (x % 2) == 0; }

int main() {
	while (scanf("%d%d", &n, &m) != EOF) {
		memset(adj, -1, sizeof adj);
		etot = 0;
		int s = 0, t = n * m + 1, sum = 0, c;
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= m; ++j) {
				scanf("%d", &c);
				sum += c;
				int p = (i - 1) * m + j;
				if (even(i + j)) link(s, p, c); else link(p, t, c);
			}
		}
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= m; ++j) {
				if (even(i + j)) {
					for (int k = 0; k < 4; ++k) {
						int x = i + way[k][0], y = j + way[k][1];
						if (1 <= x && x <= n && 1 <= y && y <= m)
							link((i - 1) * m + j, (x - 1) * m + y, inf);
					}
				}
			}
		}
		printf("%d\n", sum - isap(s,t));
	}
	return 0;
}
