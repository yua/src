#include <cstdio>
#include <cstring>
#include <queue>
#include <vector>
#include <algorithm>

using namespace std;

const int maxn = 3e5 + 10;

vector<int> epool[maxn];

struct node {
    node *ch[2], *p;
    int det, val, max;
    bool rev;
    inline bool dir() { return this == p->ch[1]; }
    inline bool isroot() { return !(this == p->ch[0] || this == p->ch[1]); }
    inline void add(int x) {
        det += x;
        val += x;
        max += x;
    }
    inline void relax();
    inline void update() { max = std::max(val, std::max(ch[0]->max, ch[1]->max)); }
} pool[maxn], *tot, *nil;

inline void node::relax() {
    if (det) {
        if (ch[0] != nil) ch[0]->add(det);
        if (ch[1] != nil) ch[1]->add(det);
        det = 0;
    }
    if (rev) {
        swap(ch[0], ch[1]);
        if (ch[0] != nil) ch[0]->rev ^= 1;
        if (ch[1] != nil) ch[1]->rev ^= 1;
        rev = false;
    }
}

inline node* makenode(int x) {
    tot->max = tot->val = x;
    tot->det = tot->rev = 0;
    tot->ch[0] = tot->ch[1] = tot->p = nil;
    return tot++;
}

void rotate(node *u) {
    node *p = u->p;
    p->relax(), u->relax();
    bool d = u->dir();
    u->p = p->p;
    if (!p->isroot()) p->p->ch[p->dir()] = u;
    if (u->ch[d^1] != nil) u->ch[d^1]->p = p;
    p->p = u, p->ch[d] = u->ch[d^1], u->ch[d^1] = p;
    p->update(), u->update();
}

void splay(node *u) {
    u->relax();
    while (u->p != nil && !u->isroot()) {
        if (u->p->isroot())
            rotate(u);
        else
            u->dir() == u->p->dir() ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
    }
    u->update();
}

node* expose(node *u) {
    node *v;
    for (v = nil; u != nil; u = u->p) {
        splay(u);
        u->ch[1] = v;
        (v = u)->update();
    }
    return v;
}

void evert(node *u) {
    expose(u)->rev ^= 1;
    splay(u);
}

bool sametree(node *u, node *v) {
    while (u->p != nil) u = u->p;
    while (v->p != nil) v = v->p;
    return u == v;
}

void link(node *u, node *v) {
    if (sametree(u, v)) {
        puts("-1");
        return;
    }
    evert(u);
    u->p = v;
    expose(u);
}

void cut(node *u, node *v) {
    if (u == v || !sametree(u, v)) {
        puts("-1");
        return;
    }
    evert(u);
    expose(v);
    splay(v);
    v->ch[0]->p = nil;
    v->ch[0] = nil;
    v->update();
}

void mark(node *u, node *v, int w) {
    if (!sametree(u, v)) {
        puts("-1");
        return;
    }
    evert(u);
    expose(v);
    splay(v);
    v->add(w);
}

int query(node *u, node *v) {
    if (!sametree(u, v)) return -1;
    evert(u);
    expose(v);
    splay(v);
    return v->max;
}

node *id[maxn];

void bfs(int s) {
    static queue<int> q;
    static bool vis[maxn];
    memset(vis, 0, sizeof vis);
    q.push(s), vis[s] = true;
    while (!q.empty()) {
        int a = q.front();
        q.pop();
        for (int i = 0; i < epool[a].size(); ++i) {
            int b = epool[a][i];
            if (!vis[b]) {
                q.push(b);
                vis[b] = true;
                id[b]->p = id[a];
            }
        }
    }
}

int main() {
#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    int n;
    nil = new node;
    nil->ch[0] = nil->ch[1] = nil->p = nil;
    nil->val = nil->det = nil->max = 0;
    nil->rev = false;
    while (scanf("%d", &n) != EOF) {
        for (int i = 1; i <= n; ++i) epool[i].clear();
        int a, b, c;
        for (int i = 1; i < n; ++i) {
            scanf("%d%d", &a, &b);
            epool[a].push_back(b);
            epool[b].push_back(a);
        }
        tot = pool;
        for (int i = 1; i <= n; ++i) {
            scanf("%d", &a);
            id[i] = makenode(a);
        }
        bfs(1);
        int m, op;
        scanf("%d", &m);
        while (m--) {
            scanf("%d%d%d", &op, &a, &b);
            switch (op) {
                case 1:
                    link(id[a], id[b]);
                    break;
                case 2:
                    cut(id[a], id[b]);
                    break;
                case 3:
                    scanf("%d", &c);
                    mark(id[b], id[c], a);
                    break;
                case 4:
                    printf("%d\n", query(id[a], id[b]));
                    break;
            }
        }
        putchar('\n');
    }
    delete nil;
    return 0;
}
