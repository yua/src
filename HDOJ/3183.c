#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

#define maxn 1010
#define lg(x) (log(x)/log(2.0))

char s[maxn];
int n,m,num[maxn],table[maxn][20];

int min(int p, int q) {
	return num[p] <= num[q] ? p : q;
}

void init() {
	int i,j;
	memset(table,0,sizeof table);
	for (i=1; i<=n; ++i) table[i][0] = i;
	for (j=1; (1<<j)<=n; ++j)
		for (i=1; i+(1<<j)-1<=n; ++i)
			table[i][j] = min(table[i][j-1],table[i+(1<<(j-1))][j-1]);
}

int query(int l, int r) {
	int k = lg(r-l+1);
	return min(table[l][k],table[r-(1<<k)+1][k]);
}

int main() {
	int i;
	while (scanf(" %s%d",s,&m) != EOF) {
		n = strlen(s);
		memset(num,0,sizeof num);
		for (i=0; i<n; ++i) num[i+1] = s[i] - '0';
		init();
		bool flag = false;
		int cur = 1;
		for (i=1; i<=(n-m); ++i) {
			int x = query(cur,i+m);
			cur = x+1;
			if (num[x]) flag = true;
			if (flag) putchar(num[x]+'0');
		}
		if (!flag) putchar('0');
		putchar('\n');
	}
	return 0;
}
