#include <stdio.h>
#include <stdbool.h>

#define maxn 110

int data[maxn][maxn],n,q,opt[maxn][maxn];

bool visit[maxn];

struct BinaryTree
{
	int l,r,sum;
} tree[maxn];

int max(int p, int q)
{
	return p>q?p:q;
}

void buildtree(int num)
{
	visit[num]=true;
	int i;
	for (i=1; i<=n; i++)
		if (data[num][i] && !visit[i])
		{
			buildtree(i);
			tree[num].l=tree[num].r;
			tree[num].r=i;
			tree[num].sum+=tree[i].sum+1;
		}
}

int mdfs(int num, int keep)
{
	if (!opt[num][keep]) 
	{
		if (keep>tree[num].sum || !keep) opt[num][keep]=0; else
		{
			opt[num][keep]=mdfs(tree[num].l,keep-1)+data[num][tree[num].l];
			opt[num][keep]=max(opt[num][keep],mdfs(tree[num].r,keep-1)+data[num][tree[num].r]);
			int i;
			for (i=0; i<=keep-2; i++)
				opt[num][keep]=max(opt[num][keep],mdfs(tree[num].l,i)+mdfs(tree[num].r,keep-2-i)+
				                   data[num][tree[num].l]+data[num][tree[num].r]);
		}
	}
	return opt[num][keep];
}

int main()
{
	scanf("%d%d",&n,&q);
	int l,r,v,i;
	for (i=1; i<n; i++)
	{
		scanf("%d%d%d",&l,&r,&v);
		data[l][r]=v;
		data[r][l]=v;
	}
	buildtree(1);
	printf("%d\n",mdfs(1,q));
	return 0;
}