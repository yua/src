{$inline on}

const
  maxn=101;

var
  n,i,p,q:longint;
  edge:array [0..maxn*maxn] of record
                             adj,next:longint;
                           end;
  ehead,mark,dfn,father,low:array [0..maxn] of longint;
  cut:array [0..maxn] of boolean;
  tot,ans:longint;

function min(p,q:longint):longint; inline;
begin
  if p<q then exit(p) else exit(q);
end;

procedure insedge(p,q:longint);
begin
  inc(tot);
  edge[tot].adj:=q;
  edge[tot].next:=ehead[p];
  ehead[p]:=tot;
end;

procedure dfs(root,dep:longint);
var
  p,q,tot:longint;
  isleaf:boolean;

begin
  p:=ehead[root];
  low[root]:=dep;
  dfn[root]:=low[root];
  mark[root]:=1;
  tot:=0;
  while p>0 do
    begin
      q:=edge[p].adj;
      if (q<>father[root]) and (mark[q]=1) then low[root]:=min(low[root],dfn[q]);
      if mark[q]=0 then
        begin
          father[q]:=root;
          dfs(q,dep+1);
          inc(tot);
          low[root]:=min(low[root],low[q]);
          if ((root=1) and (tot>1)) or ((root>1) and (low[q]>=dfn[root])) then cut[root]:=true;
        end;
      p:=edge[p].next;
    end;
  mark[root]:=2;
end;

begin
  readln(n);
  while n<>0 do
    begin
      read(p);
      tot:=0;
      fillchar(ehead,sizeof(ehead),0);
      while p<>0 do
        begin
          while not eoln do
            begin
              read(q);
              insedge(p,q);
              insedge(q,p);
            end;
          read(p);
        end;
      fillchar(mark,sizeof(mark),0);
      fillchar(father,sizeof(father),0);
      fillchar(cut,sizeof(cut),false);
      dfs(1,1);
      ans:=0;
      for i:=1 to n do inc(ans,ord(cut[i]));
      writeln(ans);
      read(n);
    end;
end.
