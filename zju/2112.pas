type
  treapnode=record
              v,p,size,l,r:longint;
              //v: value, p: priority, l: lson, r: rson
            end;

  segment=record
            l,r:longint;
            bst:longint; //bst: root of the treap
            lson,rson:longint;
          end;

var
  treap:array [0..1100000] of treapnode;
  segtree:array [0..100100] of segment;
  pos,top,x,root:longint;
  data:array [0..50010] of longint;

//Implementation of Treap

procedure new_treap(v,root:longint);
begin
  treap[root].p:=random(maxlongint);
  treap[root].l:=0;
  treap[root].r:=0;
  treap[root].v:=v;
  treap[root].size:=1;
end;

procedure RightRotate(var p:longint);
var
  q:longint;

begin
  q:=treap[p].l;
  treap[p].l:=treap[q].r;
  treap[q].r:=p;
  p:=q;
end;

procedure LeftRotate(var p:longint);
var
  q:longint;

begin
  q:=treap[p].r;
  treap[p].r:=treap[q].l;
  treap[q].l:=p;
  p:=q;
end;

procedure update(p:longint);
begin
  if p=0 then exit;
  treap[p].size:=treap[treap[p].l].size+treap[treap[p].r].size+1;
end;

procedure add(v:longint; var root:longint); overload;
begin
  if root=0 then
    begin
      inc(pos);
      root:=pos;
      new_treap(v,root);
      exit;
    end;
  if v<treap[root].v then
    begin
      add(v,treap[root].l);
      if treap[treap[root].l].p<treap[root].p then RightRotate(root);
    end
  else
    begin
      add(v,treap[root].r);
      if treap[treap[root].r].p<treap[root].p then LeftRotate(root);
    end;
  update(root);
  update(treap[root].l);
  update(treap[root].r);
end;

procedure del(k:longint; var root:longint);
begin
  if k<treap[root].v then del(k,treap[root].l) else
    if k>treap[root].v then del(k,treap[root].r) else
      begin
        if (treap[root].l=0) and (treap[root].r=0) then
          begin
            root:=0;
            exit;
          end;
        if (treap[root].l>0) and (treap[root].r=0) then root:=treap[root].l else
          if (treap[root].l=0) and (treap[root].r>0) then root:=treap[root].r else
            begin
              if treap[treap[root].l].p<treap[root].p then
                begin
                  RightRotate(root);
                  del(k,treap[root].r);
                end
              else
                begin
                  LeftRotate(root);
                  del(k,treap[root].l);
                end;
            end;
      end;
  update(root);
  update(treap[root].l);
  update(treap[root].r);
end;

function succ(k,root:longint):longint;
begin
  if root=0 then exit(0);
  if k>=treap[root].v then exit(treap[treap[root].l].size+1+succ(k,treap[root].r));
  exit(succ(k,treap[root].l));
end;

//end of Treap

//Implementation of SegmentTree

function mid(root:longint):longint; inline;
begin
  exit((segtree[root].l+segtree[root].r) shr 1);
end;

procedure BuildTree(l,r:longint; var root:longint);
var
  i:longint;

begin
  inc(top);
  root:=top;
  segtree[root].l:=l;
  segtree[root].r:=r;
  segtree[root].bst:=0;
  segtree[root].lson:=0;
  segtree[root].rson:=0;
  for i:=l to r do add(data[i],segtree[root].bst);
  if l=r then exit;
  BuildTree(l,mid(root),segtree[root].lson);
  BuildTree(mid(root)+1,r,segtree[root].rson);
end;

procedure change(p,v,root:longint); inline;
begin
  del(p,segtree[root].bst);
  add(v,segtree[root].bst);
end;

procedure modify(p,v,root:longint); overload;
begin
  change(data[p],v,root);
  if segtree[root].l=segtree[root].r then exit;
  if p<=mid(root) then modify(p,v,segtree[root].lson) else modify(p,v,segtree[root].rson);
end;

function askrank(l,r,v,root:longint):longint;
begin
  if (segtree[root].l>=l) and (segtree[root].r<=r) then exit(succ(v,segtree[root].bst));
  if r<=mid(root) then exit(askrank(l,r,v,segtree[root].lson)) else
    if l>mid(root) then exit(askrank(l,r,v,segtree[root].rson)) else
      exit(askrank(l,r,v,segtree[root].lson)+askrank(l,r,v,segtree[root].rson));
end;

function ask(l,r,v:longint):longint; overload;
var
  p,q,mid,t1,t2:longint;

begin
  p:=0;
  q:=maxlongint shr 1;
  while p<=q do
    begin
      mid:=(p+q) shr 1;
      t1:=askrank(l,r,mid,1);
      t2:=askrank(l,r,mid-1,1);
      if (v<=t1) and (v>t2) then exit(mid);
      if t1<v then p:=mid+1 else q:=mid;
    end;
end;

procedure modify(p,v:longint); overload;
begin
  modify(p,v,1);
  data[p]:=v;
end;

procedure main;
var
  n,m,i,x,y,k:longint;
  order:char;

begin
  pos:=0;
  top:=0;
  readln(n,m);
  for i:=1 to n do read(data[i]);
  readln;
  root:=1;
  BuildTree(1,n,root);
  repeat
    read(order);
    case order of
      'C':begin
            readln(x,k);
            modify(x,k);
          end;
      'Q':begin
            readln(x,y,k);
            writeln(ask(x,y,k));
          end;
    end;
    dec(m);
  until m=0;
end;

begin
  randomize;
  readln(x);
  repeat
    dec(x);
    main;
  until x=0;
end.
