#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define maxn 10010
#define maxk 210

int main()
{
	int count;
	scanf("%d",&count);
	int a[maxn];
	bool f[maxn][maxk];
	while (count--) 
	{
		int n,k;
		scanf("%d%d",&n,&k);
		memset(f,false,sizeof(f));
		memset(a,0,sizeof(a));
		int i,j;
		for (i=1; i<=n; i++) 
		{
			scanf("%d",&(a[i]));
			a[i]%=k;
		}
		f[1][a[1]%k+k]=true;
		for (i=1; i<n; i++) 
			for (j=1-k; j<k; j++) 
				if (f[i][j+k]) 
				{
					f[i+1][(j+a[i+1])%k+k]=true;
					f[i+1][(j-a[i+1])%k+k]=true;
				}
		if (f[n][k]) printf("Divisible"); else printf("Not divisible");
		if (count) printf("\n\n");
	}
	return 0;
}