#include <cstdio>
#include <cstring>
#include <algorithm>

const int maxn=1000+10;

int n,data[maxn];

int dp(int num[]) {
	static int ans[maxn];
	ans[1]=num[1];
	int len=1;
	for (int i=2; i<=n; ++i) {
		if (num[i] > ans[len]) ans[++len]=num[i]; else {
			int *pos=std::lower_bound(ans,ans+len,num[i]);
			*pos = num[i];
		}
	}
	return len;
}

int main() {
	int tcase;
	std::scanf("%d",&tcase);
	while (tcase--) {
		std::scanf("%d",&n);
		for (int i=1; i<=n; ++i) std::scanf("%d",&data[i]);
		std::printf("%d\n",dp(data));
		if (tcase) std::putchar('\n');
	}
	return 0;
}
