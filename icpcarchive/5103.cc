#include <cstdio>
#include <cstring>
#include <queue>
#include <algorithm>

struct node
{
	int fail,flag,next[26];
} ac[500000];

char s[5100001];

int count,len;

inline void insert(char *s)
{
	int len=std::strlen(s),p=0;
	for (int i=0; i<len; ++i)
	{
		int tmp=s[i]-'A';
		if (!ac[p].next[tmp]) ac[p].next[tmp]=++count;
		p=ac[p].next[tmp];
	}
	ac[p].flag=1;
}

void build()
{
	ac[0].fail=-1;
	std::queue<int> q;
	q.push(0);
	while (!q.empty())
	{
		int u=q.front();
		q.pop();
		for (int i=0; i<26; i++)
		{
			int p=ac[u].next[i],tmp;
			if (p)
			{
				for (tmp=ac[u].fail; tmp!=-1; tmp=ac[tmp].fail)
					if (ac[tmp].next[i])
					{
						ac[p].fail=ac[tmp].next[i];
						break;
					}
				if (tmp==-1) ac[p].fail=0;
				q.push(p);
			}
		}
	}
}

int solve()
{
	int p=0,len=std::strlen(s),ans=0;
	for (int i=0; i<len; i++)
	{
		int tmp=s[i]-'A';
		while (p && !ac[p].next[tmp]) p=ac[p].fail;
		p=ac[p].next[tmp];
		int t=p;
		while (t && ac[t].flag!=-1)
		{
			ans+=ac[t].flag;
			ac[t].flag=-1;
			t=ac[t].fail;
		}
	}
	return ans;
}

void init()
{
	std::memset(ac,0,sizeof(ac));
	std::memset(s,0,sizeof(s));
	char st[1001];
	int n;
	count=0;
	scanf("%d",&n);
	for (int i=0; i<n; ++i)
	{
		scanf("%s",st);
		insert(st);
	}
	scanf("\n");
	char ch;
	len=0;
	for (scanf("%c",&ch); ch!='\n'; scanf("%c",&ch))
	{
		if (ch=='[')
		{
			int tmp;
			scanf("%d",&tmp);
			scanf("%c",&ch);
			while (tmp--) s[len++]=ch;
			scanf("%c",&ch);
		}
		else s[len++]=ch;	
	}
	s[len]='\0';
	build();
}

int main()
{
	int t;
	scanf("%d",&t);
	while (t--)
	{
		init();
		int ans=solve();
		for (int i=0; i<len/2; ++i) std::swap(s[i],s[len-i-1]);
		ans+=solve();
		printf("%d\n",ans);
	}
}
