#pragma comment(linker, "/STACK:16777216")
#include <stdio.h>
#define maxn 100000

typedef long long int64;

int cnt=0;

struct Segment {
	int l,r;
	int64 sum,add;
	struct Segment *ch[2];
} pool[maxn*8],*T,*nil;

typedef struct Segment Segment;

void down(Segment *T) {
	if (T->add) {
		T->ch[0]->add+=T->add;
		T->ch[1]->add+=T->add;
		T->ch[0]->sum+=((T->ch[0]->r - T->ch[0]->l + 1)*T->add);
		T->ch[1]->sum+=((T->ch[1]->r - T->ch[1]->l + 1)*T->add);
		T->add=0;
	}
}

Segment* Init(int l, int r) {
	Segment *T=&pool[cnt++];
	T->l=l;
	T->r=r;
	if (l==r) {
		scanf("%lld",&T->sum);
		T->ch[0]=T->ch[1]=nil;
		return T;
	}
	int mid=(l+r)>>1;
	T->ch[0]=Init(l,mid);
	T->ch[1]=Init(mid+1,r);
	T->sum = T->ch[0]->sum + T->ch[1]->sum;
	return T;
}

int64 Query(Segment *T, int l, int r) {
	if (l <= T->l && T->r <= r) return T->sum;
	down(T);
	int mid=(T->l + T->r)>>1;
	int64 res=0;
	if (l<=mid) res+=Query(T->ch[0],l,r);
	if (r>mid) res+=Query(T->ch[1],l,r);
	return res;
}

void Modify(Segment *T, int l, int r, int64 val) {
	if (l <= T->l && T->r <= r) {
		T->sum+=(T->r - T->l + 1)*val;
		T->add+=val;
		return;
	}
	down(T);
	int mid=(T->l + T->r)>>1;
	if (l<=mid) Modify(T->ch[0],l,r,val);
	if (r>mid) Modify(T->ch[1],l,r,val);
	T->sum = T->ch[0]->sum + T->ch[1]->sum;
}

int main() {
	int n,m,l,r;
	int64 val;
	char opt;
	scanf("%d%d",&n,&m);
	nil=&pool[cnt++];
	T=Init(1,n);
	while (m--) {
		scanf(" %c %d %d",&opt,&l,&r);
		switch (opt) {
			case 'Q':
				printf("%lld\n",Query(T,l,r));
				break;
			case 'C':
				scanf("%lld",&val);
				Modify(T,l,r,val);
				break;
		}
	}
	return 0;
}
