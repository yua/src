#include <cstdio>
#include <cstring>
#include <set>
#include <utility>
#include <algorithm>

const int maxn=5001,maxm=10001;

int dis[maxn],low[maxn],vis[maxn],ehead[maxn];
int cnt,tot,n,m;

struct
{
	int adj,next;
} edge[maxm<<1];

void insedge(int p, int q)
{
	edge[cnt].adj=q;
	edge[cnt].next=ehead[p];
	ehead[p]=cnt++;
}

void dfs(int fa, int i)
{
	vis[i]=1;
	dis[i]=low[i]=++tot;
	for (int p=ehead[i]; p!=-1; p=edge[p].next)
	{
		int j=edge[p].adj;
		if (!vis[j])
		{
			dfs(i,j);
			low[i]=std::min(low[i],low[j]);
		}
		else if (vis[j]==1 && j!=fa) low[i]=std::min(low[i],dis[j]);
	}
	vis[i]=2;
}

int main()
{
	scanf("%d%d",&n,&m);
	cnt=tot=0;
	memset(ehead,255,sizeof(ehead)); //ehead={-1}
	int p,q;
	std::set< std::pair<int,int> > hash;
	while (m--)
	{
		scanf("%d%d",&p,&q);
		if (hash.find(std::make_pair(p,q))!=hash.end()) continue;
		hash.insert(std::make_pair(p,q));
		hash.insert(std::make_pair(q,p));
		insedge(p,q);
		insedge(q,p);
	}
	dfs(-1,1);
	int degree[maxn]={0};
	for (int i=1; i<=n; ++i)
		for (int j=ehead[i]; j!=-1; j=edge[j].next)
			if (low[i] != low[edge[j].adj]) ++degree[low[i]];
	int ans=0;
	for (int i=1; i<=n; ++i) ans+=(degree[i]==1);
	printf("%d\n",(ans+1)/2);
	return 0;
}
