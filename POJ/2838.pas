{$inline on}

const
  maxn=1000;
  maxm=20000;

type
  node=record
         s,t:longint;
       end;

var
  opt:char;
  n,tcase,i,u,v,cnt:longint;
  epool:array [0..maxm] of node;
  f:array [0..maxn] of longint;

procedure init; inline;
var
  i:longint;

begin
  for i:=1 to n do f[i]:=i;
end;

function find(x:longint):longint;
begin
  if x=f[x] then exit(x);
  f[x]:=find(f[x]);
  exit(f[x]);
end;

begin
  readln(n,tcase);
  cnt:=0;
  init;
  repeat
    dec(tcase);
    readln(opt,u,v);
    case opt of
      'I':
        begin
          inc(cnt);
          with epool[cnt] do
            begin
              s:=u;
              t:=v;
            end;
          u:=find(u);
          v:=find(v);
          if u<>v then f[u]:=v;
        end;
      'D':
        begin
          for i:=1 to cnt do
            with epool[i] do
              if ((s=u) and (t=v)) or ((t=u) and (s=v)) then
                begin
                  s:=0;
                  t:=0;
                  break;
                end;
          init;
          for i:=1 to cnt do
            with epool[i] do
              if (s>0) and (t>0) then f[find(s)]:=find(t);
        end;
      'Q':if find(u)=find(v) then writeln('Y') else writeln('N');
    end;
  until tcase=0;
end.