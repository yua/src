type
  trie=^node;
  node=record
         next:array ['0'..'9'] of trie;
         sum:longint;
       end;

var
  n,t:longint;
  s:string[10];
  flag:boolean;
  root:trie;

procedure init(var root:trie);
begin
  new(root);
  fillchar(root^.next,sizeof(root^.next),0);
  root^.sum:=1;
end;

procedure insert(s:string[10]);
var
  i:longint;
  p:trie;

begin
  p:=root;
  i:=0;
  while i<length(s) do
    begin
      inc(i);
      if p^.next[s[i]]=nil then init(p^.next[s[i]]) else inc(p^.next[s[i]]^.sum);
      p:=p^.next[s[i]];
    end;
end;

procedure release(var root:trie);
var
  ch:char;

begin
  for ch:='0' to '9' do
    if root^.next[ch]<>nil then release(root^.next[ch]);
  dispose(root);
  root:=nil;
end;

function check(root:trie):boolean;
var
  ch:char;
  tmp:longint;

begin
  check:=true;
  tmp:=0;
  for ch:='0' to '9' do
    if root^.next[ch]<>nil then
      begin
        check:=check and check(root^.next[ch]);
        inc(tmp,root^.next[ch]^.sum);
      end;
  if (root^.sum=tmp) or (root^.sum=1) then exit;
  check:=false;
end;

begin
  readln(t);
  repeat
    dec(t);
    init(root);
    readln(n);
    repeat
      dec(n);
      readln(s);
      insert(s);
    until n=0;
    if check(root) then writeln('YES') else writeln('NO');
    release(root);
  until t=0;
end.