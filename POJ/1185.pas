const
  maxn=100+10;
  maxstate=60+10;

var
  state,sum:array [0..maxstate] of longint;
  dp:array [0..maxn,0..maxstate,0..maxstate] of longint;
  cur:array [0..maxn] of longint;
  n,m,tot,i,j,k,l:longint;
  ans:longint;

function max(p,q:longint):longint; inline; begin if p>q then exit(p); exit(q); end;
function ok(p,q:longint):boolean; inline; begin exit((p and q)=0); end;

function bitcount(x:longint):longint;
begin
  x := (x and $55555555) + ((x shr 1) and $55555555);
  x := (x and $33333333) + ((x shr 2) and $33333333);
  x := (x and $0F0F0F0F) + ((x shr 4) and $0F0F0F0F);
  x := (x and $00FF00FF) + ((x shr 8) and $00FF00FF);
  x := (x and $0000FFFF) + ((x shr 16) and $0000FFFF); 
  exit(x);
end;

procedure init;
var
  i,j:longint;
  ch:char;

begin
  readln(n,m);
  for i:=1 to n do
    begin
      for j:=1 to m do
        begin
          read(ch);
          if ch='H' then cur[i]:=cur[i] or (1 shl (j-1));
        end;
      readln;
    end;
  for i:=0 to 1 shl m-1 do
    if (ok(i,i shl 1)) and (ok(i,i shl 2)) then 
      begin
        inc(tot);
        state[tot]:=i;
        sum[tot]:=bitcount(i);
      end;
  fillchar(dp,sizeof(dp),255);
  for i:=1 to tot do
    if ok(state[i],cur[1]) then dp[1,i,1]:=sum[i];
end;

begin
  init;
  for i:=2 to n do
    begin
      for j:=1 to tot do
        begin
          if not ok(cur[i],state[j]) then continue;
          for k:=1 to tot do
            begin
              if not ok(state[j],state[k]) then continue;
              for l:=1 to tot do
                begin
                  if not (ok(state[j],state[l]) and ok(state[k],state[l])) then continue;
                  if dp[i-1,k,l]=-1 then continue;
                  dp[i,j,k]:=max(dp[i,j,k],dp[i-1,k,l]+sum[j]);
                end;
            end;
        end;
    end;
  ans:=0;
  for i:=1 to tot do
    for j:=1 to tot do
      ans:=max(ans,dp[n,i,j]);
  writeln(ans);
end.
