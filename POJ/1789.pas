const
  maxn=2000;

var
  adj:array [0..maxn,0..maxn] of longint;
  st:array [0..maxn] of string;
  dis:array [0..maxn] of longint;
  vis:array [0..maxn] of boolean;
  i,j,k,n,ans:longint;

begin
  readln(n);
  while n>0 do
    begin
      fillchar(adj,sizeof(adj),0);
      for i:=1 to n do
        begin
          readln(st[i]);
          for j:=1 to i do
            begin
              for k:=1 to 7 do
                if st[i][k]<>st[j][k] then inc(adj[i,j]);
              adj[j,i]:=adj[i,j];
            end;
        end;
      fillchar(dis,sizeof(dis),$3F);
      fillchar(vis,sizeof(vis),false);
      ans:=0;
      dis[1]:=0;
      for i:=1 to n do
        begin
          k:=0;
          for j:=1 to n do
            if (not vis[j]) and ((k=0) or (dis[j]<dis[k])) then k:=j;
          vis[k]:=true;
          inc(ans,dis[k]);
          dis[k]:=0;
          for j:=1 to n do
            if adj[k,j]<dis[j] then dis[j]:=adj[k,j];
        end;
      writeln('The highest possible quality is 1/',ans,'.');
      readln(n);
    end;
end.
