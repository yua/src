var
  n,i:longint;
  x,y,l,r:longint;

begin
  readln(n);
  for i:=1 to n do
    begin
      readln(x,y);
      l:=0;
      r:=0;
      while (x>1) and (y>1) do
        begin
          if x>y then
            begin
              inc(l,x div y);
              x:=x mod y;
            end
          else
            begin
              inc(r,y div x);
              y:=y mod x;
            end;
        end;
      if (x=1) and (y>1) then inc(r,y-1);
      if (x>1) and (y=1) then inc(l,x-1);
      writeln('Scenario #',i,':');
      writeln(l,' ',r);
      writeln;
    end;
end.
