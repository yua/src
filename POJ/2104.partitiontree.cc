#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn=100001;

int sorted[maxn];
int toLeft[20][maxn],val[20][maxn];

void build(int l, int r, int d, int k)
//d:dep, k:order
{
	if (l==r) return;
	int mid=l+r>>1,lsame=mid-l+1; 
	for (int i=l; i<=r; ++i) if (val[d][i]<sorted[mid]) --lsame;
	int lpos=l,rpos=mid+1,same=0;
	for (int i=l; i<=r; ++i)
	{
		if (i==l) toLeft[d][i]=0; else toLeft[d][i]=toLeft[d][i-1];
		if (val[d][i]<sorted[mid])
		{
			++toLeft[d][i];
			val[d+1][lpos++]=val[d][i];
		}
		else if (val[d][i]>sorted[mid]) val[d+1][rpos++]=val[d][i];
		else
		{
			if (same<lsame)
			{
				++same,++toLeft[d][i];
				val[d+1][lpos++]=val[d][i];
			}
			else val[d+1][rpos++]=val[d][i];
		}
	}
	build(l,mid,d+1,k<<1);
	build(mid+1,r,d+1,k<<1|1);
}

int query(int l, int r, int p, int q, int rank, int d, int k)
{
	if (l==r) return val[d][l];
	int s,ss;
	if (l==p) s=toLeft[d][r],ss=0; else 
		s=toLeft[d][r]-toLeft[d][l-1],ss=toLeft[d][l-1];
	int m=p+q>>1;
	if (s>=rank)
	{
		int newl=p+ss,newr=p+ss+s-1;
		return query(newl,newr,p,m,rank,d+1,k<<1);
	}
	else
	{
		int bb=l-p-ss,b=r-l+1-s;
		int newl=m+bb+1,newr=m+bb+b;
		return query(newl,newr,m+1,q,rank-s,d+1,k<<1|1);
	}
}

int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for (int i=1; i<=n; ++i) scanf("%d",&sorted[i]);
	memcpy(val[0],sorted,sizeof(sorted));
	sort(sorted+1,sorted+n+1);
	build(1,n,0,1);
	while (m--)
	{
		int l,r,k;
		scanf("%d%d%d",&l,&r,&k);
		printf("%d\n",query(l,r,1,n,k,0,1));
	}
	return 0;
}
