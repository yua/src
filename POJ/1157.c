#include <stdio.h>
#include <limits.h>

#define maxn 110
#define __max(p,q) ((p)>(q)?(p):(q))

int main() {
	int f,v,a[maxn][maxn]; //f:flower, v:vase
	scanf("%d%d",&f,&v);
	int i,j,k;
	for (i=1; i<=f; i++)
		for (j=1; j<=v; j++)
			scanf("%d",&(a[i][j]));
	int opt[maxn][maxn]={0};
	for (i=1; i<=f; i++)
		for (j=i; j<=v; j++)
		{
			opt[i][j]=INT_MIN;
			for (k=i-1; k<j; k++) opt[i][j]=__max(opt[i][j],opt[i-1][k]);
			opt[i][j]+=a[i][j];
		}
	int ans=INT_MIN;
	for (j=f; j<=v; j++) ans=__max(ans,opt[f][j]);
	printf("%d\n",ans);
	return 0;
}