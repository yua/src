#include <cstdio>
#include <cstring>
#include <algorithm>

#define in(x) scanf("%d",&x)
#define out(x) printf("%d\n",x)

using namespace std;

const int maxn = 200 + 10, maxm = maxn*maxn, inf = 1e9;

int n, m;

int to[maxm], next[maxm], cap[maxm], etot;
int adj[maxn];

void link(int a, int b, int c) {
	to[etot] = b;
	next[etot] = adj[a];
	cap[etot] = c;
	adj[a] = etot++;
	to[etot] = a;
	next[etot] = adj[b];
	cap[etot] = 0;
	adj[b] = etot++;
}

int h[maxn], gap[maxn];

int dfs(int a, int df, int s, int t) {
	if (a == t) return df;
	for (int i = adj[a]; i != -1; i = next[i]) {
		int b = to[i];
		if (cap[i] && h[a] == h[b] + 1) {
			int f = dfs(b, min(df, cap[i]), s, t);
			if (f) {
				cap[i] -= f;
				cap[i^1] += f;
				return f;
			}
		}
	}
	if (--gap[h[a]] == 0) h[s] = n;
	++gap[++h[a]];
	return 0;
}

int isap(int s, int t) {
	memset(h, 0, sizeof h);
	memset(gap, 0, sizeof gap);
	int flow = 0;
	while (h[s] < n) flow += dfs(s, inf, s, t);
	return flow;
}

int main() {
	while (in(m) != EOF) {
		in(n);
		memset(adj, -1, sizeof adj);
		etot = 0;
		for (int i = 1, a, b, c; i <= m; ++i) {
			in(a), in(b), in(c);
			link(a, b, c);
		}
		out(isap(1, n));
	}
	return 0;
}
