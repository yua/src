type
  trie=^node;
  node=record
         next:array ['a'..'z'] of trie;
         sum:longint;
       end;

var
  s:array [0..1000] of string[20];
  root:trie;
  n,i,ans:longint;

procedure init(var root:trie);
begin
  new(root);
  fillchar(root^.next,sizeof(root^.next),0);
  root^.sum:=1;
end;

procedure insert(s:string[20]);
var
  i,k:longint;
  p:trie;

begin
  p:=root;
  i:=0;
  while i<length(s) do
    begin
      inc(i);
      if p^.next[s[i]]=nil then init(p^.next[s[i]]) else inc(p^.next[s[i]]^.sum);
      p:=p^.next[s[i]];
    end;
end;

function query(s:string[20]):longint;
var
  i:longint;
  p:trie;

begin
  i:=0;
  p:=root;
  while i<length(s) do
    begin
      inc(i);
      p:=p^.next[s[i]];
      if p^.sum=1 then exit(i);
    end;
end;

begin
  init(root);
  while not eof do
    begin
      inc(n);
      readln(s[n]);
      insert(s[n]);
    end;
  for i:=1 to n do writeln(s[i],' ',copy(s[i],1,query(s[i])));
  dispose(root);
end.
