#include <cstdio>
#include <cctype>
#include <cstring>

using namespace std;

const int maxn=1010;

int n;
bool tree[maxn][maxn];

inline int lowbit(int x)
{
	return x & ( x ^ (x-1) );
}

inline void rev(int x, int y)
{
	if (!x || !y) return;
	for (; x; x-=lowbit(x))
	{
		int tmp=y;
		for (; y; y-=lowbit(y)) tree[x][y]^=1;
		y=tmp;
	}
}

inline void ins(int x, int y, int p, int q)
{
	rev(p,q);
	rev(x-1,q);
	rev(p,y-1);
	rev(x-1,y-1);
}

inline int query(int p, int q)
{
	bool result=false;
	for (; p<=n; p+=lowbit(p))
	{
		int t=q;
		for (; q<=n; q+=lowbit(q)) result^=tree[p][q];
		q=t;
	}
	return result;
}

int main()
{
	int x;
	scanf("%d",&x);
	while (x--) 
	{
		int t;
		scanf("%d%d",&n,&t);
		char order;
		memset(tree,false,sizeof(tree));
		while (t--)
		{
			for (order='\0'; !isupper(order); order=getchar());
			switch (order)
			{
				case 'C':
					{
						int x,y,p,q;
						scanf("%d%d%d%d",&x,&y,&p,&q);
						ins(x,y,p,q);
						break;
					}
				case 'Q':
					{
						int x,y;
						scanf("%d%d",&x,&y);
						printf("%d\n",query(x,y));
						break;
					}
			}
		}
		if (x) printf("\n");
	}
	return 0;
}
