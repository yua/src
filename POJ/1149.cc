#include <cstdio>
#include <cstring>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

const int maxn = 110, maxm = 1010, inf = 1e9, maxe = maxn*maxn;

int n,m,s = 0,t,pig[maxm],last[maxm];

int to[maxe],next[maxe],cap[maxe],etot = 0;
int adj[maxn];

void link(int p, int q, int c) {
	to[etot] = q;
	next[etot] = adj[p];
	cap[etot] = c;
	adj[p] = etot++;
	to[etot] = p;
	next[etot] = adj[q];
	cap[etot] = 0;
	adj[q] = etot++;
}

int gap[maxn],h[maxn];

int dfs(int i, int minf) {
	if (i == t) return minf;
	for (int p = adj[i]; p != -1; p = next[p]) {
		int j = to[p];
		if (cap[p] && h[i] == h[j]+1) {
			int f = dfs(j,min(minf,cap[p]));
			if (f) {
				cap[p] -= f;
				cap[p^1] += f;
				return f;
			}
		}
	}
	if (--gap[h[i]] == 0) h[s] = n+2;
	++gap[++h[i]];
	return 0;
}

int isap(int s, int t) {
	memset(h,0,sizeof h);
	memset(gap,0,sizeof gap);
	int res = 0;
//	gap[0] = n+2;
	while (h[s] < n+2) res += dfs(s,inf);
	return res;
}

int main() {
	scanf("%d%d",&m,&n);
	t = n+1;
	for (int i = 1; i <= m; ++i) scanf("%d",pig+i);
	memset(adj,-1,sizeof adj);
	for (int i = 1,p,q; i <= n; ++i) {
		scanf("%d",&p);
		for (int j = 1; j <= p; ++j) {
			scanf("%d",&q);
			if (last[q])
				link(last[q],i,inf);
			else
				link(s,last[q] = i,pig[q]);
		}
		scanf("%d",&p);
		link(i,t,p);
	}
	printf("%d\n",isap(s,t));
	return 0;
}
