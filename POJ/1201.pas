const
  maxn=50000;

var
  n,i,cnt,a,b,c,r:longint;
  edge:array [0..maxn*10] of record
                               src,dest,cost:longint;
                             end;
  ehead,dis:array [0..maxn] of longint;

procedure insert_edge(p,q,c:longint);
begin
  inc(cnt);
  edge[cnt].src:=p;
  edge[cnt].dest:=q;
  edge[cnt].cost:=c;
end;

function bellman_ford:longint;
var
  i,j:longint;
  flag:boolean;

begin
  fillchar(dis,sizeof(dis),127);
  dis[0]:=0;
  for i:=1 to r do
    begin
      flag:=false;
      for j:=1 to cnt do
        if dis[edge[j].src]+edge[j].cost<dis[edge[j].dest] then
          begin
            dis[edge[j].dest]:=dis[edge[j].src]+edge[j].cost;
            flag:=true;
          end;
      if not flag then break;
    end;
  exit(dis[r]-dis[1]);
end;

begin
  readln(n);
  fillchar(ehead,sizeof(ehead),255);
  for i:=1 to n do
    begin
      readln(a,b,c);
      if b+1>r then r:=b+1;
      insert_edge(b+1,a,-c);
    end;
  for i:=1 to r do
    begin
      insert_edge(0,i,0);
      insert_edge(i,i+1,1);
      insert_edge(i+1,i,0);
    end;
  writeln(bellman_ford);
end.