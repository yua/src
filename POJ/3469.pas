{$inline on}

const
  maxm=500000;
  maxn=200020;

var
  n,m,cnt,i,a,b,c,flow,ans,src,dest:longint;
  ehead,dep:array [0..maxn] of longint;
  edge:array [0..maxm] of record
                            adj,next,cost:longint;
                          end;

procedure insert_edge(s,t,c:longint); inline;
begin
  edge[cnt].adj:=t;
  edge[cnt].next:=ehead[s];
  edge[cnt].cost:=c;
  ehead[s]:=cnt;
  inc(cnt);
end;

function min(p,q:longint):longint; inline;
begin
  if p<q then exit(p) else exit(q);
end;

function bfs(s,t:longint):boolean;
var
  head,tail,i,p,j,w:longint;
  queue:array [0..maxn] of longint;

begin
  fillchar(dep,sizeof(dep),255);
  head:=0;
  tail:=1;
  queue[tail]:=s;
  dep[s]:=0;
  repeat
    inc(head);
    i:=queue[head];
    p:=ehead[i];
    while p<>-1 do
      begin
        j:=edge[p].adj;
        w:=edge[p].cost;
        if (w>0) and (dep[j]=-1) then
          begin
            dep[j]:=dep[i]+1;
            inc(tail);
            queue[tail]:=j;
            if j=t then exit(true);
          end;
        p:=edge[p].next;
      end;
  until head>=tail;
  exit(false);
end;

function dfs(now,c:longint):longint;
var
  i,p,w,tmp,sum:longint;

begin
  if now=dest then exit(c);
  p:=ehead[now];
  sum:=0;
  while p<>-1 do
    begin
      i:=edge[p].adj;
      w:=edge[p].cost;
      if (w>0) and (dep[now]+1=dep[i]) and (sum<c) then
        begin
          tmp:=dfs(i,min(c-sum,w));
          if tmp>0 then
            begin
              dec(edge[p].cost,tmp);
              inc(edge[p xor 1].cost,tmp);
              inc(sum,tmp);
            end;
        end;
      p:=edge[p].next;
    end;
  if sum=0 then dep[now]:=-1;
  exit(sum);
end;

begin
  while not eof do
    begin
      readln(n,m);
      cnt:=0;
      fillchar(ehead,sizeof(ehead),255);
      src:=0;
      dest:=n+1;
      for i:=1 to n do
        begin
          readln(a,b);
          insert_edge(src,i,a);
          insert_edge(i,src,0);
          insert_edge(i,dest,b);
          insert_edge(dest,i,0);
        end;
      for i:=1 to m do
        begin
          readln(a,b,c);
          insert_edge(a,b,c);
          insert_edge(b,a,c);
        end;
      ans:=0;
      while bfs(src,dest) do
        repeat
          flow:=dfs(src,maxlongint);
          if flow=0 then break;
          inc(ans,flow);
        until false;
      writeln(ans);
    end;
end.
