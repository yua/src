const
  maxn=1024;

type
  subnode=record
            l,r:integer;
            value:longint;
          end;
  node=record
         l,r:integer;
         sub:array [0..maxn shl 2] of subnode;
       end;

var
  n,order,count,t:longint;
  tree:array [0..maxn shl 2] of node;
  x,y,p,q:integer;

procedure sub_build(l,r:integer; root,subroot:longint);
var
  m:integer;

begin
  tree[root].sub[subroot].value:=0;
  tree[root].sub[subroot].l:=l;
  tree[root].sub[subroot].r:=r;
  if l=r then exit;
  m:=(l+r) shr 1;
  sub_build(l,m,root,subroot shl 1);
  sub_build(m+1,r,root,subroot shl 1+1);
end;

procedure build(l,r:integer; root:longint);
var
  m:integer;

begin
  tree[root].l:=l;
  tree[root].r:=r;
  if l<>r then
    begin
      m:=(l+r) shr 1;
      build(l,m,root shl 1);
      build(m+1,r,root shl 1+1);
    end;
  sub_build(0,n-1,root,1);
end;

procedure sub_modify(p:integer; t,root,subroot:longint);
var
  m:integer;

begin
  inc(tree[root].sub[subroot].value,t);
  if tree[root].sub[subroot].l=tree[root].sub[subroot].r then exit;
  m:=(tree[root].sub[subroot].l+tree[root].sub[subroot].r) shr 1;
  if p<=m then sub_modify(p,t,root,subroot shl 1) else
    sub_modify(p,t,root,subroot shl 1+1);
end;

procedure modify(p,q:integer; t,root:longint);
var
  m:integer;

begin
  sub_modify(q,t,root,1);
  if tree[root].l=tree[root].r then exit;
  m:=(tree[root].l+tree[root].r) shr 1;
  if p<=m then modify(p,q,t,root shl 1) else modify(p,q,t,root shl 1+1);
end;

function sub_query(x,y:integer; root,subroot:longint):longint;
var
  m:integer;

begin
  if (tree[root].sub[subroot].l=x) and (tree[root].sub[subroot].r=y) then
    exit(tree[root].sub[subroot].value);
  m:=(tree[root].sub[subroot].l+tree[root].sub[subroot].r) shr 1;
  if y<=m then exit(sub_query(x,y,root,subroot shl 1)) else
    if x>m then exit(sub_query(x,y,root,subroot shl 1+1)) else
      exit(sub_query(x,m,root,subroot shl 1)+sub_query(m+1,y,root,subroot shl 1+1));
end;

function query(p,q,x,y:integer; root:longint):longint;
var
  m:integer;

begin
  if (tree[root].l=p) and (tree[root].r=q) then
    exit(sub_query(x,y,root,1));
  m:=(tree[root].l+tree[root].r) shr 1;
  if q<=m then exit(query(p,q,x,y,root shl 1)) else
    if p>m then exit(query(p,q,x,y,root shl 1+1)) else
      exit(query(p,m,x,y,root shl 1)+query(m+1,q,x,y,root shl 1+1));
end;

begin
  readln(n,n);
  build(0,n-1,1);
  repeat
    read(order);
    if order=1 then
      begin
        readln(p,q,t);
        modify(p,q,t,1);
      end;
    if order=2 then
      begin
        readln(x,y,p,q);
        writeln(query(x,p,y,q,1));
      end;
  until order=3;
end.
