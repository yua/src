const
  maxn=1050;

type
  subnode=record
            l,r:integer;
            flag:boolean;
          end;
  node=record
         l,r:integer;
         sub:array [0..maxn shl 2] of subnode;
       end;

var
  n,count,t:longint;
  tree:array [0..maxn shl 2] of node;
  x,y,p,q:integer;
  order:char;

procedure sub_build(l,r:integer; root,subroot:longint);
var
  m:integer;

begin
  tree[root].sub[subroot].flag:=false;
  tree[root].sub[subroot].l:=l;
  tree[root].sub[subroot].r:=r;
  if l=r then exit;
  m:=(l+r) shr 1;
  sub_build(l,m,root,subroot shl 1);
  sub_build(m+1,r,root,subroot shl 1+1);
end;

procedure build(l,r:integer; root:longint);
var
  m:integer;

begin
  tree[root].l:=l;
  tree[root].r:=r;
  if l<>r then
    begin
      m:=(l+r) shr 1;
      build(l,m,root shl 1);
      build(m+1,r,root shl 1+1);
    end;
  sub_build(1,n,root,1);
end;

procedure sub_modify(p,q:integer; root,subroot:longint);
var
  m:integer;

begin
  if (tree[root].sub[subroot].l=p) and (tree[root].sub[subroot].r=q) then
    begin
      tree[root].sub[subroot].flag:=not tree[root].sub[subroot].flag;
      exit;
    end;
  m:=(tree[root].sub[subroot].l+tree[root].sub[subroot].r) shr 1;
  if q<=m then sub_modify(p,q,root,subroot shl 1) else
    if p>m then sub_modify(p,q,root,subroot shl 1+1) else
      begin
        sub_modify(p,m,root,subroot shl 1);
        sub_modify(m+1,q,root,subroot shl 1+1);
      end;
end;

procedure modify(l,r,p,q:integer; root:longint);
var
  m:integer;

begin
  if (l=tree[root].l) and (r=tree[root].r) then
    begin
      sub_modify(p,q,root,1);
      exit;
    end;
  m:=(tree[root].l+tree[root].r) shr 1;
  if r<=m then modify(l,r,p,q,root shl 1) else
    if l>m then modify(l,r,p,q,root shl 1+1) else
      begin
        modify(l,m,p,q,root shl 1);
        modify(m+1,r,p,q,root shl 1+1);
      end;
end;

function sub_query(x:integer; root,subroot:longint):boolean;
var
  m:integer;

begin
  if tree[root].sub[subroot].l=tree[root].sub[subroot].r then
    exit(tree[root].sub[subroot].flag);
  m:=(tree[root].sub[subroot].l+tree[root].sub[subroot].r) shr 1;
  if x<=m then exit(tree[root].sub[subroot].flag xor sub_query(x,root,subroot shl 1)) else
    exit(tree[root].sub[subroot].flag xor sub_query(x,root,subroot shl 1+1));
end;

function query(x,y:integer; root:longint):boolean;
var
  m:integer;
  tmp:boolean;

begin
  tmp:=sub_query(y,root,1);
  if (tree[root].l=x) and (tree[root].r=x) then exit(tmp);
  m:=(tree[root].l+tree[root].r) shr 1;
  if x<=m then exit(tmp xor query(x,y,root shl 1)) else
    exit(tmp xor query(x,y,root shl 1+1));
end;

begin
  readln(count);
  repeat
    dec(count);
    readln(n,t);
    build(1,n,1);
    repeat
      dec(t);
      read(order,x,y);
      if order='C' then
        begin
          readln(p,q);
          modify(x,p,y,q,1);
        end
      else
        begin
          readln;
          writeln(ord(query(x,y,1)));
        end;
    until t=0;
    if count>0 then writeln;
  until count=0;
end.
