type
  pair=record
         q_num,period:longint;
       end;

var
  heap:array [0..10010] of pair;
  data:array [0..3010] of pair;
  tmp:pair;
  count,i,j,k,top,p:longint;
  st:string;

operator >(a,b:pair) c:boolean;
begin
  c:=a.period>b.period;
end;

operator <(a,b:pair) c:boolean;
begin
  c:=a.period<b.period;
end;

procedure swap(var p,q:pair);
var
  t:pair;

begin
  t:=p;
  p:=q;
  q:=t;
end;

procedure up(k:longint);
begin
  if k=1 then exit;
  if heap[k]>heap[k shr 1] then
    begin
      swap(heap[k],heap[k shr 1]);
      up(k shr 1);
    end;
end;

procedure down(k:longint);
var
  j:longint;

begin
  if k shl 1>top then exit;
  j:=k shl 1;
  if (heap[j+1]>heap[j]) and (j<top) then inc(j);
  if heap[k]<heap[j] then
    begin
      swap(heap[j],heap[k]);
      down(j);
    end;
end;

procedure pop;
begin
  heap[1]:=heap[top];
  dec(top);
  down(1);
end;

procedure push(p:pair);
begin
  inc(top);
  heap[top]:=p;
  up(top);
end;

procedure sort(l,r:longint);
var
  i,j:longint;
  m:pair;

begin
  i:=l;
  j:=r;
  m:=heap[(l+r) shr 1];
  repeat
    while (heap[i].period<m.period) or ((heap[i].period=m.period) and (heap[i].q_num<m.q_num)) do
      inc(i);
    while (heap[j].period>m.period) or ((heap[j].period=m.period) and (heap[j].q_num>m.q_num)) do
      dec(j);
    if i<=j then
      begin
        swap(heap[i],heap[j]);
        inc(i);
        dec(j);
      end;
  until i>j;
  if l<j then sort(l,j);
  if r>i then sort(i,r);
end;

begin
  readln(st);
  count:=0;
  while st<>'#' do
    begin
      inc(count);
      p:=pos(' ',st);
      delete(st,1,p);
      p:=pos(' ',st);
      val(copy(st,1,p-1),data[count].q_num);
      val(copy(st,p+1,length(st)),data[count].period);
      readln(st);
    end;
  readln(k);
  tmp:=data[1];
  tmp.period:=0;
  for j:=1 to k do
    begin
      inc(tmp.period,data[1].period);
      push(tmp);
    end;
  for i:=2 to count do
    begin
      tmp:=data[i];
      tmp.period:=0;
      for j:=1 to k do
        begin
          inc(tmp.period,data[i].period);
          if tmp.period<heap[1].period then
            begin
              pop;
              push(tmp);
            end;
        end;
    end;
  sort(1,k);
  for i:=1 to k do writeln(heap[i].q_num);
end.
