#include <cstdio>
#include <climits>
#include <algorithm>

using namespace std;

const int maxn=20010;

int n,m,l=INT_MIN,r=0,a[2][maxn];

inline void readln(int a[])
{
	for (int i=1; i<=n; ++i) scanf("%d",&a[i]),l=max(l,a[i]),r+=a[i];
}

bool ok(int ans)
{
	static int dis[2][maxn],both[maxn],f[maxn];
	int c,p,sum,i,p1,p2;
	for (p=sum=0,i=1; i<=n; ++i)
	{
		sum+=a[0][i]+a[1][i];
		while (sum>ans && p!=i)
		{
			++p;
			sum-=(a[0][p]+a[1][p]);
		}
		both[i]=p;
	}
	for (p=sum=0,i=1; i<=n; ++i)
	{
		sum+=a[0][i];
		while (sum>ans && p!=i) sum-=a[0][++p];
		dis[0][i]=p;
		if (p==i) return false;
	}
	for (p=sum=0,i=1; i<=n; ++i)
	{
		sum+=a[1][i];
		while (sum>ans && p!=i) sum-=a[1][++p];
		dis[1][i]=p;
		if (p==i) return false;
	}
	f[0]=0;
	for (i=1; i<=n; ++i)
	{
		f[i]=INT_MAX>>2;
		if (both[i]!=i) f[i]=min(f[i],f[both[i]]+1);
		p1=p2=i,c=0;
		while (p1 || p2)
		{
			++c;
			p1 > p2 ? p1=dis[0][p1] : p2=dis[1][p2];
			f[i]=min(f[i],(p1>p2 ? f[p1] : f[p2])+c);
		}
		if (f[i]>m) return false;
	}
	return true;
}

int main()
{
	scanf("%d%d",&n,&m);
	readln(a[0]);
	readln(a[1]);
	int mid=(l+r)>>1;
	for (--l; l+1<r; mid=(l+r)>>1) ok(mid) ? r=mid : l=mid;
	printf("%d\n",r);
	return 0;
}
