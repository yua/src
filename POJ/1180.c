/*
f,t: prefix sum

dp[i] = min{dp[j] + (f[i] - f[j]) * t[i] + (f[n] - f[j]) * s}

dp[k] + (- f[k]) * t[i] + (- f[k]) * s < dp[j] + (- f[j]) * t[i] + (- f[j]) * s

y(k) = dp[k] - f[k] * s
x(k) = f[k]

y(k) - y(j) < (x(k) - x(j)) * t[i]

j < k ( x(j) < x(k) )

slope < t[i]

j > k ( x(j) > x(k) )

slope > t[i]
*/
#include <stdio.h>

#define maxn 10010

typedef long long int64;

int64 n,s,t[maxn],f[maxn],dp[maxn],deque[maxn];

inline int64 y(int k) { return dp[k] - f[k]*s; }
inline int64 x(int k) { return f[k]; }

inline double slope(int p, int q) {
	return (y(p) - y(q)) / (double)(x(p) - x(q));
}

int main() {
	scanf("%lld%lld",&n,&s);
	int i;
	for (i = 1; i <= n; ++i) scanf("%lld%lld",t+i,f+i);
	for (i = 1; i <= n; ++i) t[i] += t[i-1], f[i] += f[i-1];
	int64 *qf,*qr;
	qf = qr = &deque[0];
	for (i = 1; i <= n; ++i) {
		while (qf < qr && slope(*qf,*(qf+1)) < t[i]) ++qf;
		dp[i] = dp[*qf] + (f[i] - f[*qf]) * t[i] + (f[n] - f[*qf]) * s;
		while (qf < qr && slope(*(qr-1),*qr) > slope(*qr,i)) --qr;
		*++qr = i;
	}
	printf("%lld\n",dp[n]);
	return 0;
}
