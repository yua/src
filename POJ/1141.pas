const
  str_max='12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890';
  //200 chars
  maxn=200;

var
  st:string;
  opt:array [0..maxn,0..maxn] of string;
  i,j:longint;

function another(ch:char):char;
begin
  case ch of 
    '(':another:=')';
    ')':another:='(';
    '[':another:=']';
    ']':another:='[';
  end;
end;

function becomepair(ch:char):string;
begin
  becomepair:=''+ch;
  if (ch='(') or (ch='[') then becomepair:=becomepair+another(ch)
  	else becomepair:=another(ch)+becomepair;
end;

function ispair(st:string):boolean;
begin
  ispair:=false;
  if (st[1]='(') and (st[2]=')') then exit(true);
  if (st[1]='[') and (st[2]=']') then exit(true);
end;

function min(p,q:string):string; //return the shorter one
begin
  if length(p)<length(q) then exit(p) else exit(q);
end;

function dfs(l,r:longint):string;
var
  k:longint;

begin
  if l>r then exit('');
  if l=r then exit(becomepair(st[l]));
  if opt[l,r]<>str_max then exit(opt[l,r]);
  if ispair(st[l]+st[r]) then 
    opt[l,r]:=min(opt[l,r],st[l]+dfs(l+1,r-1)+st[r]);
  if (st[l]='(') or (st[l]='[') then
    opt[l,r]:=min(opt[l,r],st[l]+dfs(l+1,r)+another(st[l]));
  if (st[r]=')') or (st[r]=']') then
    opt[l,r]:=min(opt[l,r],another(st[r])+dfs(l,r-1)+st[r]);
  for k:=l to r-1 do 
    opt[l,r]:=min(opt[l,r],dfs(l,k)+dfs(k+1,r));
  exit(opt[l,r]);
end;

begin
  readln(st);
  for i:=1 to length(st) do 
    for j:=1 to length(st) do 
      opt[i][j]:=str_max;
  writeln(dfs(1,length(st)));
end.