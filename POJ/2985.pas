const
  maxn=200000+10;

var
  n,m,i,j,k,l,r,op,mid,num:longint;
  size,f,a:array [0..maxn] of longint;

//Implementation of merge-find set

function find(x:longint):longint;
begin
  if x=f[x] then exit(x);
  f[x]:=find(f[x]);
  exit(f[x]);
end;

//End of merge-find set

//Implementation of BIT

function lowbit(x:longint):longint;
begin
  exit(x and -x);
end;

procedure add(k,num:longint);
begin
  while k<=n do
    begin
      inc(a[k],num);
      inc(k,lowbit(k));
    end;
end;

function sum(k:longint):longint;
begin
  sum:=0;
  while k>0 do
    begin
      inc(sum,a[k]);
      dec(k,lowbit(k));
    end;
end;

//End of BIT

begin
  readln(n,m);
  for i:=1 to n do
    begin
      size[i]:=1;
      f[i]:=i;
    end;
  add(1,n);
  num:=n;
  repeat
    dec(m);
    read(op,i);
    case op of
      0:begin
          read(j);
          i:=find(i);
          j:=find(j);
          if i<>j then
            begin
              add(size[i],-1);
              add(size[j],-1);
              inc(size[i],size[j]);
              f[j]:=i;
              add(size[i],1);
              dec(num);
            end;
        end;
      1:begin
          k:=num-i+1;
          l:=1;
          r:=n;
          while l<=r do
            begin
              mid:=(l+r) shr 1;
              if sum(mid)>=k then r:=mid-1 else l:=mid+1;
            end;
          writeln(l);
        end;
    end;
  until m=0;
end.
