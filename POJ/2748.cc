#include <cstdio>
#include <algorithm>

using namespace std;

const int maxn=150000;

int f[maxn]={0,1,1,2}; 

int main()
{
	int tcase;
	long long n;
	scanf("%d",&tcase);
	for (int i=3; i<maxn; ++i) f[i]=(f[i-1]+f[i-2])%100000;
	while (tcase--)
	{
		scanf("%lld",&n);
		--n<<=1;
		printf("%d\n",f[++n%maxn]);
	}
	return 0;
}
