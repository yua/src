import java.io.*;
import java.math.*;
import java.util.*;

public class Main
{
	public static void main(String[] args)
	{
		Scanner stdin = new Scanner(new BufferedInputStream(System.in));
		int n=stdin.nextInt();
		for(int i=0; i<n; ++i)
		{
			BigDecimal a = stdin.nextBigDecimal();
			BigDecimal b = stdin.nextBigDecimal();
			System.out.println(a.add(b));
		}
	}
}
