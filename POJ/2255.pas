type
  tree=^node;
  node=record
         ch:char;
         lson,rson:tree;
       end;

var
  root:tree;
  s,preorder,inorder:string;
  tmp:longint;
  ch:char;

procedure build(preorder,inorder:string; var root:tree);
var
  s1,s2:string;
  a1,a2:string;
  p:longint;

begin
  root^.ch:=preorder[1];
  if length(preorder)<=1 then
    begin
      root^.lson:=nil;
      root^.rson:=nil;
      if length(preorder)=0 then root^.ch:='#';
      exit;
    end;
  p:=pos(preorder[1],inorder);
  s1:=copy(inorder,1,p-1);
  s2:=copy(inorder,p+1,length(inorder));
  new(root^.lson);
  new(root^.rson);
  a1:=copy(preorder,2,p-1);
  a2:=copy(preorder,length(preorder)-length(s2)+1,length(s2));
  build(a1,s1,root^.lson);
  build(a2,s2,root^.rson);
end;

procedure postorder(root:tree);
begin
  if root=nil then exit;
  postorder(root^.lson);
  postorder(root^.rson);
  if root^.ch<>'#' then write(root^.ch);
end;

begin
  while not eof do
    begin
      readln(s);
      tmp:=pos(' ',s);
      preorder:=copy(s,1,tmp-1);
      inorder:=copy(s,tmp+1,length(s));
      new(root);
      build(preorder,inorder,root);
      postorder(root);
      writeln;
      dispose(root);
    end;
end.
