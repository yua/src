package main

import "fmt"

func main() {
	tmp := 0
	flag:=true

	for {
		_,err := fmt.Scanln(&tmp)
		if err != nil {
			break
		}
		if flag && tmp != 42 {
			fmt.Println(tmp)
		} else {
			flag = false
		}
	}
}
