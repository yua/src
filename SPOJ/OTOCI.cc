#include <cstdio>
#include <algorithm>

#define REP(i,n) for (int i=1; i<=n; ++i)

const int maxn = 30000 + 10;

class node
{
	static node* nil;
	#define nil node::nil
	
	node *ch[2],*p;
	int sum,val;
	bool rev;
	
	inline int sgn()
	{
		return p->ch[0] == this ? 0 : p->ch[1] == this ? 1 : -1;
	}
	
	inline void sc(int d, node* c) { ch[d] = c; c->p = this; }
	
	inline void update()
	{
		sum = ch[0]->sum + ch[1]->sum + val;
	}
	
	inline void relax()
	{
		if (rev)
		{
			std::swap(ch[0],ch[1]);
			ch[0]->rev ^= 1, ch[1]->rev ^= 1;
			rev = 0;
		}
	}
	
	inline void rotate(int d)
	{
		node *x = p, *y = x->p;
		if (~x->sgn()) y->sc(x->sgn(), this); else p = y;
		x->sc(d, ch[d^1]), sc(d^1,x);
		x->update();
	}
	
	inline void rotate() { rotate(sgn()); }
	inline void zig() { rotate(0); }
	inline void zag() { rotate(1); }
	
	inline void fix()
	{
		if (~sgn()) p->fix();
		relax();
	}
	
	inline node* splay()
	{
		fix();
		while (~sgn()) rotate();
		update();
		return this;
	}
	
	inline node* expose()
	{
		node *x = this, *y = nil;
		do
		{
			x->splay();
			x->ch[1] = y, x->update();
			y = x, x = x->p;
		} while (x != nil);
		return y;
	}
	
	inline node* root()
	{
		node *x;
		for (x = expose(); x->relax(), x->ch[0] != nil; x = x->ch[0]);
		return x;
	}
	
	inline node* evert()
	{
		expose()->rev ^= 1;
		return this;
	}
	
public:
	inline node(int v = 0): p(nil), val(v)
	{
		ch[0] = ch[1] = nil;
		sum = rev = 0;
	}
	
	bool link(node *v)
	{
		if (root() == v->root()) return false; else
		{
			evert(), splay();
			p = v;
			return true;
		}
	}
	
	void modify(int x) { splay(), val = x; }
	
	int query(node *x)
	{
		if (root() != x->root()) return -1; else
		{
			expose();
			node *y = nil;
			int res = 0;
			do
			{
				x->splay();
				if (x->p == nil) return x->ch[1]->sum + x->val + y->sum;
				x->ch[1] = y, x->update();
				y = x, x = x->p;
			} while (x != nil);
		}
	}
} *v[maxn], *nil = new node();

int main()
{
	int n,tmp;
	std::scanf("%d",&n);
	REP(i,n)
		std::scanf("%d",&tmp), v[i] = new node(tmp);
	int x,y,q;
	char opt[20];
	std::scanf("%d",&q);
	while (q--)
	{
		std::scanf(" %s %d %d",opt,&x,&y);
		int ans;
		switch (opt[0])
		{
			case 'b':
				v[x]->link(v[y]) ? std::puts("yes") : std::puts("no");
				break;
			case 'p':
				v[x]->modify(y);
				break;
			case 'e':
				ans = v[x]->query(v[y]);
				ans == -1 ? std::puts("impossible") : std::printf("%d\n",ans);
				break;
		}
	}
	return 0;
}
